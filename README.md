# Kuno Fundraisers

A non-custodial Monero fundraising platform.

**Warning: This code is amateur-level, messy and not fully tested.**

## Why Monero?

Monero is "cash for the internet" – a fast, secure and inclusive way to send and receive money worldwide.

No bank account, application or government ID is required to use Monero.

Monero can easily be exchanged for physical cash (e.g. LocalMonero.co), virtual debit cards and gift cards (e.g. CakePay.com) or spent on products and services (e.g. Monerica.com).

For more information, visit GetMonero.org.

## How to install

Use the following instructions to run your own instance of Kuno Fundraisers on a fresh Ubuntu or Debian VPS.

Shared hosting also works – In that case, login to cPanel, upload the files via the File Manager or FTP, manage the database via PHPMyAdmin, and then setup the cron.

**1. Install the web server**

`sudo apt install apache2 mysql-server php7.4 libapache2-mod-php7.4 php7.4-mysql php7.4-gd certbot python3-certbot-apache postfix`

Warning: Kuno currently doesn't work with PHP 8.

**2. Get a free SSL certificate**

`sudo certbot -d yourdomain.com`

**3. Setup the website**

`curl -LO https://codeberg.org/Anarkio/kuno/archive/main.zip`

`unzip kuno-main.zip`

`shopt -s dotglob`

`mv kuno/* /var/www/`

`sudo chown -R www-data:www-data /var/www/`

**4. Set the domain name**

`find /var/www/ -iname "*.php" -exec sed -i "s_kuno.bitejo.com_yourdomain.com_g" {} \;`

`sed -i "s_kuno.bitejo.com_yourdomain.com_g" /var/www/html/assets/style.css`

`sudo a2enmod rewrite`

`sudo service apache2 restart`

`sudo nano /etc/apache2/apache2.conf`

Replace:

```
<Directory /var/www/>
        Options Indexes FollowSymLinks
        AllowOverride None
        Require all granted
</Directory>
```

With:

```
<Directory /var/www/>
        Options Indexes FollowSymLinks
        AllowOverride All
        Require all granted
</Directory>
```

**5. Setup the database**

`sudo mysql`

`CREATE DATABASE kuno_db DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;`

`CREATE USER 'kuno_user'@'localhost' IDENTIFIED WITH mysql_native_password BY 'YOUR_PASSWORD_HERE';`

`GRANT ALL ON kuno_db.* TO 'kuno_user'@'localhost';`

`FLUSH PRIVILEGES;`

`EXIT;`

`sudo mysql -u kuno_user -p kuno_db < /var/www/src/db/structure.sql`

Then add the database name, username and password to `connect.php`:

`nano /var/www/src/db/connect.php`

**6. Setup cron (important, syncs donations)**

`crontab -u www-data -e`

Add this line to the end of the file:

`*/5 * * * * /usr/bin/php /var/www/src/func/cron.php fxq5Lgd1h9EwKHow12b134PQYu831s5m >/dev/null 2>&1`

**7. Done!**

## Onion Monero Blockchain Explorer

Kuno Fundraisers uses the Onion Monero Blockchain Explorer API to fetch transactions.

If you have the resources to run your own instance of this API (requires a full node, which requires around 200~ GB storage), you can download it here: https://github.com/moneroexamples/onion-monero-blockchain-explorer and add the URL of your instance to `/src/func/monero_api.php`

If not, there are 4 instances listed in `/src/func/monero_api.php`

The API returns TXs in the most recent 5 blocks. Therefore the cron is set to check each fundraiser every 5 minutes (in case a block is mined faster than average). A new function that returns blocks between two specified blockheights would make it possible to catch up on blocks if the website goes offline, as well as only sync older fundraisers on demand.

## Future ideas

- Optional monetization features for operators:
  - Featured fundraisers on the homepage and search page (payment via TXID and TX key).
  - Non-intrusive ads for KYC-free crypto platforms/merchants/media.
  - Premium features, such as digital downloads for Donation Pages (similar to Patreon) where donors can submit the TX key and download an ebook/song/channel link/chatroom invite/etc.
  - ❌ TX fees aren't possible, as fundraisers are non-custodial and donations are sent directly to the user.
  - ❌ Listing fees would be prohibitive, especially for charity/medical/living costs fundraisers, as well as Kickstarters without start capital. Even if someone could afford the fee, it may be difficult for them to acquire KYC-free Monero in their location, e.g. low cash by mail/cash in person liquidity.
- Federation feature: A global search for all fundraisers across different instances, which helps with discoverability and decentralization (similar to Mastodon/Nostr). Currently there is a RSS feed. An API and a "fediverse" tab for the homepage and search page could be added.
- Invite-only instances: If an instance is invite-only (e.g. it's for a specific niche, location or community), users will need to enter an invite code in order to publish a fundraiser. Likewise, users could host only their own fundraiser (e.g. for their software project) or Donation Page (e.g. for their Youtube channel). Anyone would be able to view and donate to the fundraisers. (There is already an option for unlisted/private fundraisers.)

## License

Kuno Fundraisers is in the public domain. Feel free to fork or use this code for your own projects.

## Disclaimer

This code is public domain and can be used for any purpose. **However there is no warranty nor guarantee.** Treat this as an experimental work in progress, instead of a production-ready finished product.
