<?php
  require_once '/var/www/src/db/connect.php';
  require_once '/var/www/src/db/fundraisers.php';
  require_once '/var/www/src/func/sanitize.php';
  function htmlent2xml($s) {
    return preg_replace_callback("/(&[a-zA-Z][a-zA-Z0-9]*;)/",function($m){
       $c = html_entity_decode($m[0],ENT_HTML5,"UTF-8");
       $convmap = array(0x80, 0xffff, 0, 0xffff);
       return mb_encode_numericentity($c, $convmap, 'UTF-8');
    },$s);
  }
  function xmlentities($aString) {
    $validChars = "A-Z0-9a-z\s_-";
    $twoChars = null;
    return preg_replace_callback("/[^$validChars]/"
      ,function ($aMatches) use(&$twoChars) { 
      $oneChar = $aMatches[0];
      switch($oneChar) {
        case "'": return "&apos;";
        case '"': return "&quot;";
        case '&': return "&amp;";
        case '<': return "&lt;";
        case '>': return "&gt;";
        default: 
          if (194 <= ord($oneChar) && ord($oneChar) <= 207) { 
            $twoChars = $oneChar;
            return;
          } else if ($twoChars) { 
            $twoChars .= $oneChar;
            $ansiChar = utf8_decode($twoChars);
            $twoChars = null;
            return "&#" . str_pad(ord($ansiChar), 3, "0", STR_PAD_LEFT) . ";";
          } else {
            return "&#" . str_pad(ord($oneChar), 3, "0", STR_PAD_LEFT) . ";";       
          }
        }
      }
    ,$aString);
  }
  $fundraisers = db_search_fundraisers($terms='all',$order_by='new',$offset=0,$count=100);
  header("Content-type: text/xml");
?>
<?xml version="1.0" encoding="UTF-8"?>
<rss version="2.0">
<channel>
  <title>Kuno – Fundraise with Monero</title>
  <link>http://kuno.bitejo.com</link>
  <description>Raise money or donate to a good cause with Monero. Peer-to-peer, KYC-free and easy-to-use.</description>
  <pubDate><?= date('Y-m-d H:i:s') ?></pubDate>
<?php
  foreach($fundraisers as $fundraiser) {
    $title = xmlentities(strip_tags(sanitize_utf8($fundraiser['title'])));
    $guid = sanitize_alphanumeric($fundraiser['guid']);
    $date = (int) $fundraiser['date'];
    $type = ($fundraiser['goal']) ? 'fundraiser' : 'donate';
    $url = 'https://kuno.bitejo.com/'.$type.'/'.$guid.'/';
    $photo = (($fundraiser['photo']) && ($fundraiser['photo'] != 'none')) ? $fundraiser['photo'] : false;
    $description = nl2br(xmlentities(strip_tags(sanitize_utf8($fundraiser['description'],8000))));
    $goal = round($fundraiser['goal'], 4);
    $total = round($fundraiser['total'], 4);
    $percentage = ceil($fundraiser['percentage'] * 100);
    $title = ($fundraiser['goal']) ? $title.' | '.$total.'/'.$goal.' XMR ('.$percentage.'%)' : $title.' | '.$total.' XMR raised';
?>
<item>
    <title><?= $title ?></title>
    <pubDate><?= date('Y-m-d H:i:s', $date) ?></pubDate>
    <link><?= $url ?></link>
    <description><![CDATA[<?= ($photo) ? '<img src="'.$photo.'" alt="" style="max-width:100%;height:auto;"/><br/>' : '' ?><?= $description ?>]]></description>
</item>
<?php } ?>
</channel>
</rss>