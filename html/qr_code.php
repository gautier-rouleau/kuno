<?php
require_once '/var/www/src/func/phpqrcode.php';
require_once '/var/www/src/func/sanitize.php';

function generate_qr_code($string) {
  $string = substr($string,0,255);
  //$qr_code = QRcode::image($string);
  $qr_code = QRcode::png($string, false, QR_ECLEVEL_Q, 4); 
  header('Content-type: image/png');
  imagepng($qr_code);
  imagedestroy($qr_code);
}

if(!$_GET['string']) {
  die();
} else if($_GET['string'] != sanitize_alphanumeric($_GET['string'])) {
  die();
} else if(strlen($_GET['string']) > 255) {
  die();
} else {
  $string = sanitize_alphanumeric($_GET['string']);
  generate_qr_code($string);
}
?>