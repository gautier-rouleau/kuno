<?php
  require_once '/var/www/src/func/header.php';
?>
<?php display_header($custom_title='Error | Kuno – Fundraise with Monero'); ?>
<div class="center six padded top bottom" style="min-height:65vh">
  <div class="center three boxed padded top bottom">
    <h1>Error</h1>
    <p class="padded">Page not found</p>
  </div>
</div>
<?php display_footer(); ?>