<?php
  require_once '/var/www/src/func/header.php';
  require_once '/var/www/src/func/sanitize.php';
  require_once '/var/www/src/db/connect.php';
  require_once '/var/www/src/db/fundraisers.php';
  require_once '/var/www/src/db/donations.php';
?>
<?php display_header(); ?>
    <main id="content" class="group" role="main">
        <div class="homepage-info kuno">
          <div class="two p-left">
            <img src="https://kuno.bitejo.com/assets/header_phone2.png" alt="" class="header-phone kuno">
          </div>
            <div class="four p-left header-desc kuno">
              <div class="header-desc-text kuno">
              <h1 class="header-h1 kuno">Easy fundraising <br>for everyone <img src="https://kuno.bitejo.com/assets/monero_icon2.png" alt="" class="header-icon-small kuno"></h1>
              <p class="header-p1 kuno">Raise money or donate to a good cause with Monero</p>
              <p class="white header-p2 kuno">✔ Start a fundraiser for free</p>
              <p class="white header-p2 kuno">✔ Receive Monero (which you can exchange to cash)</p>
              <p class="white header-p2 kuno">✔ Zero fees, works globally, no bank account required</p>
              <div>
                <div class="three header-buttons kuno">
                  <a href="https://kuno.bitejo.com/new-fundraiser/">
                    <button class="header-button2 kuno">Start a fundraiser</button>
                  </a>
                </div>
                <div class="three header-buttons kuno">
                  <a href="https://kuno.bitejo.com/search/">
                    <button class="header-button2 kuno">Browse fundraisers</button>
                  </a>
                </div>
                </div>
              </div>
            </div>
          </div>
          <div class="six homepage-top-cta">
            <div class="center no-padding">
                <p class="white">The fair alternative to GoFundMe and Kickstarter</p>
            </div>
        </div>
        <div class="main six no-top no-bottom">
            <div class="header-homepage-gallery padded kuno">
                <h3 class="six top left valign-middle p-left header-latest-h3 kuno">
                  <span class="header-latest-span kuno">Browse fundraisers</span>
                  <a href="https://kuno.bitejo.com/search/" class="btn arrow-button header-latest-arrow kuno">►</a>
                </h3>
                <div class="six center homepage-gallery">
                    <?php
                      $random_fundraisers = db_select_random_fundraisers();
                      $random_fundraisers = array_slice($random_fundraisers, 0, 12);
                      foreach($random_fundraisers as $random_fundraiser) {
                    ?>
                    <a href="https://kuno.bitejo.com/<?= ($random_fundraiser['goal']) ? 'fundraiser' : 'donate' ?>/<?= $random_fundraiser['guid'] ?>/">
                        <div class="boxed two center valign-top homepage-product kuno">
                            <div class="homepage-product-img" style="background-image:url(<?= ($random_fundraiser['photo'] != 'none') ? $random_fundraiser['photo'] : 'https://kuno.bitejo.com/assets/placeholder.jpg' ?>)"></div>
                            <div class="homepage-product-title"> <?= $random_fundraiser['title'] ?></div>
                            <div class="homepage-product-progress kuno" style="background: linear-gradient(to right, #8fd476 <?= min(99, ceil($random_fundraiser['percentage'] * 100)) ?>%, #eee <?= min(99, ceil($random_fundraiser['percentage'] * 100)) ?>%, #eee 100%);">
                              <?= round($random_fundraiser['total'],4) ?> XMR<?= ($random_fundraiser['goal']) ? ' of '.round($random_fundraiser['goal'],4).' XMR' : '' ?>
                            </div>
                            <div class="homepage-product-price kuno">
                              <div class="three left no-padding homepage-product-support kuno"><?= (int) $random_fundraiser['supporters'] ?> supporters</div>
                              <div class="three right no-padding homepage-product-support kuno"><button class="homepage-product-button kuno">Support</button></div>
                            </div>
                        </div>
                    </a>
                    <?php } ?>
                </div>
            </div>
            <div class="top-categories six kuno">
                <div class="three left valign-top">
                    <h3 class="six top left valign-top top-categories-h3 kuno"> Top Categories</h3>
                    <ul class="three p-left no-top top-categories-ul kuno">
                    <?php
                      $top_tags = db_select_top_tags();
                      $top_tags = array_slice($top_tags, 0, 9);
                      $tags_count = 0;
                      foreach($top_tags as $tag) {
                        if(($tags_count > 0) && ($tags_count % 5 == 0)) {
                          echo '<ul class="three p-left no-top top-categories-ul kuno">';
                        }
                        $tags_count++;
                    ?>
                    <li>
                      <a href="https://kuno.bitejo.com/search/<?= sanitize_slug($tag['tags']) ?>" class="no-format top-categories-a kuno">
                        <?= ucwords($tag['tags']) ?>
                      </a>
                    </li>
                    <?php } ?>
                    <li><a href="https://kuno.bitejo.com/search/categories/" class="no-format top-categories-a kuno">All categories</a></li>
                  </ul>
                </div>
                <div class="three left valign-top">
                    <h3 class="six top left valign-top top-categories-h3 kuno"> Recent donations</h3>
                    <ul class="six p-left no-top top-categories-ul kuno">
                      <?php
                        $recent_donations = db_select_fundraisers_by_recent_donation();
                        $recent_donations = array_slice($recent_donations, 0, 5);
                        foreach($recent_donations as $recent_donation) {
                      ?>
                      <li>
                        <a href="https://kuno.bitejo.com/<?= ($recent_donation['goal']) ? 'fundraiser' : 'donate' ?>/<?= $recent_donation['guid'] ?>/" class="no-format top-categories-a kuno">
                          <?= $recent_donation['title'] ?> - <?= round($recent_donation['total'],4) ?><?= ($recent_donation['goal']) ? '/'.round($recent_donation['goal'],4).' XMR' : ' XMR' ?>
                        </a>
                      </li>
                      <?php } ?>
                    </ul>
                </div>
            </div>
            <div class="homepage-cta six">
                <div class="three center"> Launch your fundraiser in 1 click</div>
                <div class="three center"><a class="btn btn-outline button-cta kuno" href="https://kuno.bitejo.com/new-fundraiser/">Get started for free</a></div>
            </div>
            <div class="homepage-info monero-info kuno">
              <div class="p-left monero-info-section kuno">
              <div class="monero-info-point kuno">
                <h3 class="monero-info-h3 kuno">Monero is cash for the internet</h3>
                <p class="monero-info-p-bold kuno">
                 Monero is a fast and secure way to send and receive money online.
                </p>
                <p class="monero-info-p kuno">
                 No bank account, application or government ID is required. Simply download a wallet for your phone or computer to send and receive money worldwide with one click.
                </p>
                <p class="monero-info-p kuno">
                Monero’s simplicity and accessibility makes it an ideal payment method for online stores, remote work, remittances, tips, fundraisers and more.
                </p>
                <div>
                <a href="https://monero.com">
                  <button class="monero-info-button kuno">Download for mobile</button>
                </a>
                <a href="https://featherwallet.org">
                  <button class="monero-info-button kuno">Download for desktop</button>
                </a>
                </div>
              </div>
              <div class="monero-info-point kuno">
                <h3 class="monero-info-h3 kuno">Uncensorable donations</h3>
                <p class="monero-info-p-bold kuno">
                With Monero, your money belongs to you.
                </p>
                <p class="monero-info-p kuno">
Monero is software and doesn’t rely on third parties or corporations. Transactions can’t be censored, funds can’t be frozen and users can’t be deplatformed.
                </p>
                <p class="monero-info-p kuno">
                Whether you raise funds with Monero or donate to a good cause, you can have peace of mind that the recipient receives 100% of the donation.
                </p>
                <div>
                <a href="https://getmonero.org">
                  <button class="monero-info-button kuno">Official website</button>
                </a>
                <a href="https://anarkio.codeberg.page/monero">
                  <button class="monero-info-button kuno">Overview</button>
                </a>
                <a href="https://localmonero.co">
                  <button class="monero-info-button kuno">Buy Monero</button>
                </a>
                </div>
              </div>
              <div class="monero-info-point kuno">
                <h3 class="monero-info-h3 kuno">Spend it anywhere</h3>
                <p class="monero-info-p-bold kuno">
                Exchange Monero to cash, shop online or pay bills.
                </p>
                <p class="monero-info-p kuno">
                It’s easy to exchange Monero to physical cash with <a href="https://localmonero.co">LocalMonero</a>, <a href="https://bisq.network">Bisq</a> or a <a href="https://coinatmradar.com">Crypto ATM</a> (no bank account required).
                </p>
                <p class="monero-info-p kuno">
                Marketplaces like <a href="https://moneromarket.io">MoneroMarket</a> and directories like <a href="https://monerica.com">Monerica</a> and <a href="https://acceptedhere.io">AcceptedHere</a> help you to buy what you need with Monero.
                </p>
                <p class="monero-info-p kuno">
                In addition, <a href="https://cakepay.com">CakePay</a> and <a href="https://coincards.com">CoinCards</a> offer prepaid Visa cards and gift cards to thousands of businesses. Personal shoppers like <a href="https://proxysto.re">ProxyStore</a>, <a href="https://sovereignstack.tools/rerouter/">Sovereign Stack</a> and <a href="https://shopinbit.com">ShopInBit</a> offer a convenient way to pay invoices with Monero.
                </p>
                <p class="monero-info-p kuno">
                Monero’s flexibility makes it easy to raise funds to pay bills, kickstart your project or support a charity.
                </p>
                <div>
                <a href="https://kycnot.me">
                  <button class="monero-info-button kuno">Exchange to cash</button>
                </a>
                <a href="https://monerica.com">
                  <button class="monero-info-button kuno">Shop online</button>
                </a>
                <a href="https://moneromarket.io">
                  <button class="monero-info-button kuno">Marketplace</button>
                </a>
                </div>
              </div>
              </div>
            </div>
            <div class="homepage-info use-case kuno" style="border-bottom: 10px solid #ff7a00;">
              <div style="background-image: url(https://kuno.bitejo.com/assets/example1.jpg);background-size: cover;background-position: top left;height: 350px;" class="two-half p-left use-case-image kuno"></div>
              <div class="three p-left use-case-info kuno">
                <h3 class="use-case-h3 kuno">For charities</h3>
                <p class="use-case-p-bold kuno">
                  A local cat shelter needs donations for cat food and vet bills.
                </p>
                <p class="use-case-p kuno">
                  They setup a Kuno Fundraiser, share the link in social media and receive donations.
                </p>
                <p class="use-case-p kuno">
                  The shelter uses the fundraised Monero to buy Petsmart gift cards with CakePay and withdraws cash to pay the vet bill with a Crypto ATM.
                </p>
                <p class="use-case-p kuno">
                  Each donor receives an update with photos of the cats.
                </p>
              </div>
            </div>
            <div class="homepage-info use-case kuno" style="border-bottom: 10px solid #FDCA0D;">
              <div style="background-image: url(https://kuno.bitejo.com/assets/example2.jpg);background-size: cover;background-position: top left;height: 350px;" class="two-half p-left use-case-image kuno"></div>
              <div class="three p-left use-case-info kuno">
                <h3 class="use-case-h3 kuno">For individuals</h3>
                <p class="use-case-p-bold kuno">
                  Alice needs to raise money for medical bills.
                </p>
                <p class="use-case-p kuno">
                  Her daughter helps her to setup a Kuno Fundraiser and shares it with their community.
                </p>
                <p class="use-case-p kuno">
                  They collect enough money and exchange it to cash with LocalMonero.
                </p>
                <p class="use-case-p kuno">
                  Alice writes a heartfelt thank you letter for each donor as a token of appreciation.
                </p>
              </div>
            </div>
            <div class="homepage-info use-case kuno" style="border-bottom: 10px solid #ff7a00;">
              <div style="background-image: url(https://kuno.bitejo.com/assets/example3.jpg);background-size: cover;background-position: top center;height: 350px;" class="two-half p-left use-case-image kuno"></div>
              <div class="three p-left use-case-info kuno">
                <h3 class="use-case-h3 kuno">For startups</h3>
                <p class="use-case-p-bold kuno">
                  An indie dev wants to create a new game.
                </p>
                <p class="use-case-p kuno">
                  He sets up a Kuno Fundraiser and shares it with gaming communities.
                </p>
                <p class="use-case-p kuno">
                  He reaches the goal and uses the funds to hire Monero-friendly artists from MoneroMarket and buy game assets with a CakePay virtual debit card.
                </p>
                <p class="use-case-p kuno">
                  Each donor gets a free copy of the game.
                </p>
              </div>
            </div>
            <div class="homepage-info use-case kuno" style="border-bottom: 10px solid #FDCA0D;">
              <div style="background-image: url(https://kuno.bitejo.com/assets/example6.jpg);background-size: cover;background-position: top left;height: 350px;" class="two-half p-left use-case-image kuno"></div>
              <div class="three p-left use-case-info kuno">
                <h3 class="use-case-h3 kuno">For content creators</h3>
                <p class="use-case-p-bold kuno">
                  A band uploads their covers and original music to Youtube.
                </p>
                <p class="use-case-p kuno">
                  They setup a Kuno Donation Page to receive Monero donations.
                </p>
                <p class="use-case-p kuno">
                  Fans can also suggest songs or comment during livestreams by making a donation.
                </p>
                <p class="use-case-p kuno">
                  This offers a better way to sustainably monetize their content, compared to ads.
                </p>
              </div>
            </div>
            <div class="homepage-info use-case kuno" style="border-bottom: 10px solid #ff7a00;">
              <div style="background-image: url(https://kuno.bitejo.com/assets/example7.jpg);background-size: cover;background-position: center left;height: 350px;" class="two-half p-left use-case-image kuno"></div>
              <div class="three p-left use-case-info kuno">
                <h3 class="use-case-h3 kuno">For you</h3>
                <p class="use-case-p-bold kuno">
                  With Kuno, everyone can raise money for their project, cause or startup.
                </p>
                <p class="use-case-p kuno">
                  All you need is a Monero wallet and a goal.
                </p>
                <div>
                  <div class="three use-case-green-button kuno">
                    <a href="https://kuno.bitejo.com/new-fundraiser/">
                      <button class="use-case-button kuno">Start a fundraiser</button>
                    </a>
                  </div>
                  <div class="three use-case-green-button kuno">
                    <a href="https://kuno.bitejo.com/search/">
                      <button class="use-case-button kuno">Browse fundraisers</button>
                    </a>
                  </div>
                </div>
              </div>
            </div>
    </div>
    </main>
    <div style="clear:both;"></div>
<?php display_footer(); ?>