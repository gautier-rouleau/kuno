<?php
  require_once '/var/www/src/func/header.php';
  require_once '/var/www/src/func/reset_password.php';
  require_once '/var/www/src/func/ratelimit.php';
  
  $confirmation_code = ($_GET['confirmation_code']) ? sanitize_alphanumeric($_GET['confirmation_code']) : false;
  $fundraiser_guid = ($_GET['fundraiser_guid']) ? sanitize_alphanumeric($_GET['fundraiser_guid']) : false;
  $email = ($_GET['email']) ? sanitize_utf8(urldecode($_GET['email'])) : false;
  
  if($_POST['action'] == 'reset_password') {
    $check_ratelimit = check_ratelimit($_POST['validation'], $validation='reset_password', $tokens_cost=100);
    if($check_ratelimit['status'] == 'success') {
      $message = reset_password($_POST['fundraiser_guid'], $_POST['email'], $_POST['confirmation_code'], $_POST['new_password']);
    } else {
      $message = array('status'=>'error', 'data'=>$check_ratelimit['data']);
    }
  }
  
  $form_validation = init_ratelimit('reset_password');
  $page_title = 'Reset Password | Kuno – Fundraise with Monero';
  
?>
<?php display_header($page_title); ?>
    <main id="content" class="group" role="main">
        <div class="main four new-listing centered">
          <?php if($message) { ?>
          <div class="form-message message-<?= $message['status'] ?>">
            <?= ucfirst($message['status']) ?>: <?= $message['data'] ?>
          </div>
          <?php } ?>
          <div class="padded no-bottom">
            <div class="new-fundraiser-box kuno six boxed padded top center new-fundraiser-nav kuno">
              <span class="center"><a href="https://kuno.bitejo.com/new-fundraiser/">New fundraiser</a>  | <a href="https://kuno.bitejo.com/edit-fundraiser/">Edit fundraiser</a></span>
            </div>
          </div>
          <form method="post" class="padded p-left">
              <input type="hidden" name="action" value="reset_password" readonly>
              <input type="hidden" name="validation" value="<?= $form_validation ?>" readonly>
              <div class="new-fundraiser-box kuno product-details boxed padded top">
                <h2 class="section-h2 kuno bottom">Reset Password</h2>
                <p>Enter the fundraiser code and your email address to reset your password.</p>
                <p class="small gray">(If you didn’t add an email address, please contact support.)</p>
                <div class="one-half"><span class="new-fundraiser-h1 kuno">Fundraiser</span></div>
                <div class="four-half"><input class="inline fill" type="text" maxlength="255" name="fundraiser_guid" placeholder="Fundraiser code (e.g. ae21)" value="<?= ($fundraiser_guid) ? sanitize_utf8($fundraiser_guid) : sanitize_utf8($_POST['fundraiser_guid']) ?>"></div>
                <div class="one-half"><span class="new-fundraiser-h1 kuno">Email</span></div>
                <div class="four-half"><input class="inline fill" type="text" maxlength="255" name="email" placeholder="Email" value="<?= ($email) ? sanitize_utf8($email) : sanitize_utf8($_POST['email']) ?>"></div>
                <?php if($confirmation_code) { ?>
                <div class="one-half"><span class="new-fundraiser-h1 kuno">Confirmation code</span></div>
                <div class="four-half"><input class="inline fill" type="text" maxlength="255" name="confirmation_code" placeholder="Confirmation code" value="<?= ($confirmation_code) ? sanitize_utf8($confirmation_code) : sanitize_utf8($_POST['confirmation_code']) ?>"></div>
                <div class="one-half"><span class="new-fundraiser-h1 kuno">New password</span></div>
                <div class="four-half"><input class="inline fill" type="password" maxlength="255" name="new_password" placeholder="New password"></div>
                <?php } ?>
              </div>
              <div class="publish top"><input type="submit" class="six btn green-button kuno" style="padding-top:10px;padding-bottom:10px" value="Reset password"></div>
            </form>
        </div>
    </main>
    <div style="clear:both;"></div>
<?php display_footer(); ?>
