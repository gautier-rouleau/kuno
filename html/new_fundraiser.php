<?php
  require_once '/var/www/src/func/header.php';
  require_once '/var/www/src/func/fundraisers.php';
  require_once '/var/www/src/func/ratelimit.php';
  
  if($_GET['guid']) {
    $guid = sanitize_alphanumeric($_GET['guid']);
    if(!check_fundraiser_session($guid)) {
      $message = array('status'=>'error', 'data'=>'Please enter the fundraiser password');
      $_GET['guid'] = false;
      $_GET['check_password'] = true;
    } else {
      $compare_fundraiser = db_select_fundraiser_by_guid($guid);
      if(!$compare_fundraiser) {
        $message = array('status'=>'error', 'data'=>'Fundraiser not found');
        $_GET['guid'] = false;
        $_GET['check_password'] = true;
      } else {
        $fundraiser_comments = db_select_fundraiser_email_addresses($compare_fundraiser[0]['address']);
      }
    }
  }
  
  if($_GET['logout']) {
    unset($_SESSION['fundraiser_login']);
    $_GET['guid'] = false;
    $_GET['check_password'] = true;
  }
  
  if(($_POST['action'] == 'new_fundraiser') || (($_GET['guid']) && ($_POST['action'] == 'edit_fundraiser'))) {
    $check_ratelimit = check_ratelimit($_POST['validation'], $validation='new_fundraiser', $tokens_cost=100);
    if($check_ratelimit['status'] == 'success') {
      $message = publish_fundraiser($_POST, $action=$_POST['action'], $guid=$_POST['guid']);
      if($_POST['action'] == 'edit_fundraiser') {
        $compare_fundraiser = db_select_fundraiser_by_guid($guid);
      }
    } else {
      $message = array('status'=>'error', 'data'=>$check_ratelimit['data']);
    }
  }
  
  if(($_GET['guid']) && ($_POST['action'] == 'update_fundraiser_status')) {
    $check_ratelimit = check_ratelimit($_POST['validation'], $validation='new_fundraiser', $tokens_cost=20);
    if($check_ratelimit['status'] == 'success') {
      $message = update_fundraiser_status($action=$_POST['update_action'], $guid=$_POST['guid']);
      $compare_fundraiser = db_select_fundraiser_by_guid($guid);
    } else {
      $message = array('status'=>'error', 'data'=>$check_ratelimit['data']);
    }
  }
  
  if($_POST['action'] == 'check_password') {
    $check_ratelimit = check_ratelimit($_POST['validation'], $validation='new_fundraiser', $tokens_cost=20);
    if($check_ratelimit['status'] == 'success') {
      $message = check_fundraiser_password($_POST['guid'], $_POST['password']);
    } else {
      $message = array('status'=>'error', 'data'=>$check_ratelimit['data']);
    }
  }
  
  $form_validation = init_ratelimit('new_fundraiser');
  $page_title = (($_GET['guid']) || ($_GET['check_password'])) ? 'Edit Fundraiser | Kuno – Fundraise with Monero' : 'New Fundraiser | Kuno – Fundraise with Monero';
  
?>
<?php display_header($page_title); ?>
    <main id="content" class="group" role="main">
        <div class="main four new-listing centered">
          <?php if($message) { ?>
          <div class="form-message message-<?= $message['status'] ?>">
            <?= ucfirst($message['status']) ?>: <?= $message['data'] ?>
          </div>
          <?php } ?>
          <div class="padded no-bottom">
            <div class="new-fundraiser-box kuno six boxed padded top center new-fundraiser-nav kuno">
              <?php if(($_GET['check_password']) || ($_GET['guid'])) { ?>
              <span class="center"><a href="https://kuno.bitejo.com/new-fundraiser/">New fundraiser</a> | <b>Edit fundraiser</b> <?= ($_GET['guid']) ? '<a href="https://kuno.bitejo.com/logout-fundraiser/" class="faded gray">(Log out)</a>' : '' ?></span>
              <?php } else { ?>
              <span class="center"><b>New fundraiser</b> | <a href="https://kuno.bitejo.com/edit-fundraiser/">Edit fundraiser</a></span>
              <?php } ?>
            </div>
          </div>
          <?php if($_GET['check_password']) { ?>
            <form method="post" class="padded p-left">
              <input type="hidden" name="action" value="check_password" readonly>
              <input type="hidden" name="validation" value="<?= $form_validation ?>" readonly>
              <div class="new-fundraiser-box kuno product-details boxed padded top">
                <h2 class="section-h2 kuno bottom">Edit fundraiser</h2>
                <p>Enter the fundraiser code and password to edit your fundraiser.</p>
                <p class="small mono">The code is in the URL: https://kuno.bitejo.com/fundraiser/<u><b>ae21</b></u>-help-the-cats/</p>
                <div class="one-half"><span class="new-fundraiser-h1 kuno">Fundraiser</span></div>
                <div class="four-half"><input class="inline fill" type="text" maxlength="255" name="guid" placeholder="Fundraiser code (e.g. ae21)" value="<?= ($guid) ? sanitize_utf8($guid) : sanitize_utf8($_POST['guid']) ?>"></div>
                <div class="one-half"><span class="new-fundraiser-h1 kuno">Password</span></div>
                <div class="four-half"><input class="inline fill" type="password" maxlength="255" name="password" placeholder="Password"></div>
              <div class="top right">
                <a href="https://kuno.bitejo.com/reset-password/" class="small bold dark">Forgot password?</p>
              </div>
              </div>
              <div class="publish top"><input type="submit" class="six btn green-button kuno" style="padding-top:10px;padding-bottom:10px" value="Edit fundraiser"></div>
            </form>
          <?php } else { ?>
            <form enctype="multipart/form-data" method="post" class="padded p-left">
                <input type="hidden" name="action" value="<?= ($_GET['guid']) ? 'edit_fundraiser' : 'new_fundraiser' ?>" readonly>
                <input type="hidden" name="guid" value="<?= sanitize_alphanumeric($_GET['guid']) ?>" readonly>
                <input type="hidden" name="validation" value="<?= $form_validation ?>" readonly>
                <div class="new-fundraiser-box kuno product-details boxed padded top">
                    <h2 class="section-h2 kuno bottom">1.&nbsp; Fundraiser details</h2>
                    <p>Describe your fundraiser and add photos.</p>
                    <div class="one-half"><span class="new-fundraiser-h1 kuno">Title</span></div>
                    <div class="four-half"><input class="fill icon icon-edit" type="text" maxlength="80" name="title" value="<?= ($_POST['title']) ? sanitize_utf8($_POST['title']) : sanitize_utf8($compare_fundraiser[0]['title']) ?>" placeholder="Fundraiser title"></div>
                    <div class="one-half"><span class="new-fundraiser-h1 kuno">Photo</span>
                        <p class="small">(Optional)</p>
                    </div>
                    <div class="four-half"><input type="hidden" name="MAX_FILE_SIZE" value="2000000" /><input class="fill" type="file" accept="image/png,image/jpeg" name="photo"></div>
                    <div class="one-half"><span class="new-fundraiser-h1 kuno">Additional Photos</span>
                        <p class="small">(Optional)</p>
                    </div>
                    <div class="four-half">
                        <div class="two p-right"><input class="fill" type="file" accept="image/png,image/jpeg" name="secondary_photo_1"></div>
                        <div class="two p-right"><input class="fill" type="file" accept="image/png,image/jpeg" name="secondary_photo_2"></div>
                        <div class="two"><input class="fill" type="file" accept="image/png,image/jpeg" name="secondary_photo_3"></div>
                    </div>
                    <div class="one-half valign-top top"><span class="new-fundraiser-h1 kuno">Description</span></div>
                    <div class="four-half valign-top"><textarea style="height:100px" class="fill icon icon-list" name="description" maxlength="2500" rows="3" placeholder="Describe your fundraiser. Add fundraiser rewards, milestones or contact details, if necessary."><?= ($_POST['description']) ? sanitize_utf8($_POST['description'],8000) : sanitize_utf8($compare_fundraiser[0]['description'],8000) ?></textarea></div>
                    <div class="one-half"><span class="new-fundraiser-h1 kuno">Tags</span></div>
                    <div class="four-half"><input class="fill icon icon-tag" type="text" maxlength="200" name="tags" value="<?= ($_POST['tags']) ? sanitize_utf8($_POST['tags']) : sanitize_utf8($compare_fundraiser[0]['tags']) ?>" placeholder="Category, tags, etc."></div>
                </div>
                <div class="new-fundraiser-box kuno pricing boxed padded top">
                    <h2 class="section-h2 kuno bottom">2.&nbsp; Goal</h2>
                    <p>Set your goal and Monero address.</p>
                    <div class="one-half no-bottom"><span class="new-fundraiser-h1 kuno">Goal</span></div>
                    <div class="four-half no-bottom"><input class="fill icon icon-money" type="text" maxlength="20" name="goal" value="<?= ($_POST['goal']) ? sanitize_utf8($_POST['goal']) : sanitize_utf8($compare_fundraiser[0]['goal']) ?>" placeholder="Goal (XMR)"></div>
                    <div class="right fill small p-right"><label><input type="checkbox" name="is_donation_page" <?= (($_POST['is_donation_page'] == 'yes') || ($compare_fundraiser[0]['goal'] === 0)) ? 'checked' : '' ?> value="yes"> No goal – I want to make a Donation Page</label></div>
                    <div class="one-half"><span class="new-fundraiser-h1 kuno">Monero address</span></div>
                    <div class="four-half"><label class="inline"><input class="inline fill" type="text" maxlength="100" name="address" value="<?= ($_POST['address']) ? sanitize_utf8($_POST['address']) : sanitize_utf8($compare_fundraiser[0]['address']) ?>" placeholder="Example: 44AFFq5kSiGBoZ4NMDwYtN18obc8AemS33DBLWs3H7otXft3XjrpDtQGv7SqSsaBYBb98uNbr2VBBEt7f2wfn3RVGQBEP3A"></div>    
                    <div class="one-half"><span class="new-fundraiser-h1 kuno">View key</div>
                    <div class="four-half"><label class="inline"><input class="inline fill" type="text" maxlength="100" name="view_key" value="<?= ($_POST['view_key']) ? sanitize_utf8($_POST['view_key']) : sanitize_utf8($compare_fundraiser[0]['view_key']) ?>" placeholder="Example: f359631075708155cc3d92a32b75a7d02a5dcf27756707b47a2b31b21c389501"></div>    
                </div>
                <div class="new-fundraiser-box kuno options boxed padded top bottom">
                    <h2 class="section-h2 kuno bottom">3.&nbsp; Password</h2>
                    <p>Set a password to edit your fundraiser later.</p>
                    <div class="one-half"><span class="new-fundraiser-h1 kuno">Email</span></div>
                    <div class="four-half"><input class="inline fill" type="text" maxlength="255" name="email" value="<?= ($_POST['email']) ? sanitize_utf8($_POST['email']) : sanitize_utf8($compare_fundraiser[0]['email']) ?>" placeholder="Email (optional, for password resets)"></div>
                    <div class="one-half"><span class="new-fundraiser-h1 kuno">Password</span></div>
                    <div class="four-half"><input class="inline fill" type="password" maxlength="255" name="password" placeholder="Password <?= ($_GET['guid']) ? '(leave blank if no change)' : '' ?>"></div>
                </div>
                <div class="publish top"><input type="submit" class="six btn green-button kuno" style="padding-top:10px;padding-bottom:10px" value="Start fundraiser"></div>
            </form>
            <?php } ?>
            <?php if($_GET['guid']) { ?>
            <div class="padded">
              <div class="new-fundraiser-box kuno product-details boxed padded bottom">
                <h2 class="section-h2 kuno bottom">Fundraiser supporters</h2>
                <p>If a supporter has entered their email address, you will see it here (e.g. for fundraiser rewards).</p>
                <?php if(!$fundraiser_comments) { ?>
                <p class="top bottom"><i>No email submissions yet</i></p>
                <?php } else { ?>
                <div class="padded no-top bottom">
                  <?php foreach($fundraiser_comments as $comment) { ?>
                  <br>
                  <div class="padded no-bottom no-top">
                    <div><span class="gray">Email:</span> <?= $comment['email'] ?></div>
                    <div><span class="gray">Comment:</span> <?= ($comment['comment']) ? nl2br($comment['comment']) : 'No comment' ?></div>
                    <div><span class="gray">Amount:</span> <?= (float) $comment['amount'] ?> XMR</div>
                    <div><span class="gray">TXID:</span> <?= $comment['txid'] ?></div>
                    <div><span class="gray">TX key:</span> <?= $comment['tx_key'] ?></div>
                    <div><span class="gray">Date:</span> <?= date('Y-m-d H:i', $comment['date']) ?></div>
                  </div>
                <?php } ?>
              </div>
              <?php } ?>
            </div>
            <div class="new-fundraiser-box kuno product-details boxed padded bottom top">
                <h2 class="section-h2 kuno bottom">Manage fundraiser</h2>
                <?php if($compare_fundraiser[0]['status'] == 'active') { ?>
                <div class="three center">
                  <form method="post">
                    <input type="hidden" name="validation" value="<?= $form_validation ?>" readonly>
                    <input type="hidden" name="action" value="update_fundraiser_status" readonly>
                    <input type="hidden" name="update_action" value="deactivate_fundraiser" readonly>
                    <input type="hidden" name="guid" value="<?= $compare_fundraiser[0]['guid'] ?>" readonly>
                    <input type="submit" class="btn outline-button kuno" value="Deactivate Fundraiser">
                  </form>
                </div>
                <?php } else { ?>
                <div class="three center">
                  <form method="post">
                    <input type="hidden" name="validation" value="<?= $form_validation ?>" readonly>
                    <input type="hidden" name="action" value="update_fundraiser_status" readonly>
                    <input type="hidden" name="update_action" value="activate_fundraiser" readonly>
                    <input type="hidden" name="guid" value="<?= $compare_fundraiser[0]['guid'] ?>" readonly>
                    <input type="submit" class="btn outline-button kuno" value="Reactivate Fundraiser">
                  </form>
                </div>
                <?php } ?>
                <div class="three center">
                  <form method="post">
                    <input type="hidden" name="validation" value="<?= $form_validation ?>" readonly>
                    <input type="hidden" name="action" value="update_fundraiser_status" readonly>
                    <input type="hidden" name="update_action" value="delete_fundraiser" readonly>
                    <input type="hidden" name="guid" value="<?= $compare_fundraiser[0]['guid'] ?>" readonly>
                    <input type="submit" class="btn red-button kuno" value="⚠ Delete Fundraiser">
                  </form>
                </div>
            </div>
            <?php } ?>
            </div>
          </div>
        </div>
    </main>
    <div style="clear:both;"></div>
<?php display_footer(); ?>
