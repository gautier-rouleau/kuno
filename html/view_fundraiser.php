<?php
  require_once '/var/www/src/func/header.php';
  require_once '/var/www/src/func/sanitize.php';
  require_once '/var/www/src/db/connect.php';
  require_once '/var/www/src/db/fundraisers.php';
  require_once '/var/www/src/db/donations.php';
  require_once '/var/www/src/func/ratelimit.php';
  require_once '/var/www/src/func/fundraisers.php';
  
  if(!$_GET['guid']) { die(); }
  $guid = sanitize_alphanumeric($_GET['guid']);
  $fundraiser = db_select_fundraiser_by_guid($guid);
  if(!$fundraiser) { die(); }
  if(!$fundraiser[0]['goal']) {
    echo '<meta http-equiv="refresh" content="0; url=https://kuno.bitejo.com/donate/'.$guid.'/">';
    die();
  }
  $fundraiser[0]['total'] = db_count_fundraiser_donations($fundraiser[0]['address']);
  $fundraiser[0]['supporters'] = db_count_fundraiser_supporters($fundraiser[0]['address']);
  $fundraiser[0]['percentage'] = ceil(($fundraiser[0]['total'] / $fundraiser[0]['goal']) * 100);
  
  if($_POST['action'] == 'new_comment') {
    $check_ratelimit = check_ratelimit($_POST['validation'], $validation='new_comment', $tokens_cost=20);
    if($check_ratelimit['status'] == 'success') {
      $message = comment_on_fundraiser($_GET['guid'], $_POST);
    } else {
      $message = array('status'=>'error', 'data'=>$check_ratelimit['data']);
    }
  }
  $form_validation = init_ratelimit('new_comment');
  $page_title = $fundraiser[0]['title'].' | '.round($fundraiser[0]['total'],4).'/'.round($fundraiser[0]['goal'],4).' XMR | Kuno – Fundraise with Monero';
  $page_description = 'Donate with Monero: '.substr($fundraiser[0]['description'],0,80).'…';
  $page_photo = (($fundraiser[0]['photo']) && ($fundraiser[0]['photo'] != 'none')) ? $fundraiser[0]['photo'] : false;
?>
<?php display_header($page_title, $page_description, $page_photo); ?>
    <main id="content" class="group" role="main">
        <?php if($message) { ?>
        <div class="six padded top no-bottom centered">
          <div class="form-message message-<?= $message['status'] ?>">
            <?= ucfirst($message['status']) ?>: <?= $message['data'] ?>
          </div>
        </div>
        <?php } ?>
        <div class="main four padded top no-bottom centered">
            <div class="product boxed">
              <?php if(($fundraiser[0]['photo']) && ($fundraiser[0]['photo'] != 'none')) { ?>
              <div class="six product-holder kuno" style="background-image:url(<?= $fundraiser[0]['photo'] ?>)"></div>
              <?php } ?>
              <div class="product-main kuno">
                <h2 class="kuno product-h1 bottom"><?= $fundraiser[0]['title'] ?></h2>
                <div class="six">
                <div class="homepage-product-progress kuno progress-large" style="background: linear-gradient(to right, #8fd476 <?= min(99, $fundraiser[0]['percentage']) ?>%, #eee <?= min(99, $fundraiser[0]['percentage']) ?>%, #eee 100%);">
                  <?= $fundraiser[0]['percentage'] ?>% funded
                </div>
                </div>
                <div class="two product-price kuno">
                  <div class="product-stat kuno"> <?= round($fundraiser[0]['total'],4) ?> <span>XMR</span></div>
                  <div class="product-stat-desc kuno"> Raised</div>
                </div>
                <div class="two product-price kuno">
                  <div class="product-stat kuno"> <?= (int) $fundraiser[0]['supporters'] ?></span></div>
                  <div class="product-stat-desc kuno"> Supporters</div>
                </div>
                <div class="two product-price kuno">
                  <div class="product-stat kuno"> <?= round($fundraiser[0]['goal'],4) ?> <span>XMR</span></div>
                  <div class="product-stat-desc kuno"> Goal</div>
                </div>
                <div class="six"><span class="kuno product-h2 heading">Description</span></div>
                <div class="six no-top">
                  <?= nl2br($fundraiser[0]['description']) ?>
                </div>
                <?php if(($fundraiser[0]['secondary_photos']) && ($fundraiser[0]['secondary_photos'] != 'none')) { ?>
                <div class="six"><span class="kuno product-h2 heading">Photos</span></div>
                <div class="six center product-additional-photos">
                <?php
                  $secondary_photos = json_decode($fundraiser[0]['secondary_photos'], true);
                  foreach($secondary_photos as $photo) {
                    if($photo != 'none') {
                ?>
                    <div class="two center padded">
                        <a href="<?= $photo ?>" title="Full-size photo"><img src="<?= str_replace('.jpg', '_thumb.jpg', $photo) ?>" class="medium" alt=""></a>
                    </div>
                <?php
                  }
                }
                ?>
                </div>
                <?php } ?>
                <div class="six">
                    <ul class="no-bullet inline">
                    <?php
                      $tags = explode(',', $fundraiser[0]['tags']);
                      foreach($tags as $tag) {
                    ?>
                    <li class="small tag dark"><a href="/search/<?= sanitize_slug($tag) ?>/"> <?= $tag ?></a></li>
                    <?php } ?>
                    </ul>
                </div>
                <div class="six valign-top">
                    <p class="small">Published <?= date('Y-m-d', $fundraiser[0]['date']) ?></p>
                </div>
            </div>
        </div>
        </div>
        <div class="side two padded top">
            <div class="buy padded boxed top kuno donation-box">
                <h2 class="section-h2 kuno">Support</h2>
                <div class="product-qr kuno" style="background-image:url(https://kuno.bitejo.com/assets/monero_icon2.png), url(https://kuno.bitejo.com/qr/<?= $fundraiser[0]['address'] ?>.png);"></div>
                <p><b>Send any amount of Monero to:</b></p>
                <p><?= $fundraiser[0]['address'] ?></p>
                <a href="monero:<?= $fundraiser[0]['address'] ?>" class="six btn green-button kuno">Open in wallet</a>
            </div>
            <div class="reviews padded boxed top">
                <h2 class="section-h2 kuno">Comments</h2>
                <?php
                  $comments = db_select_fundraiser_comments($fundraiser[0]['address']);
                  if(!$comments) {
                ?>
                <p class="top"><i>No comments yet</i></p>
                <?php
                  } else {
                    foreach($comments as $comment) {
                ?>
                <div class="six">
                    <div class="card dark"><?= nl2br($comment['comment']); ?></div>
                    <div class="small right" style="padding-top:2px;"><?= round($comment['amount'],4); ?> XMR, <?= date('Y-m-d', $comment['date']) ?></div>
                </div>
                <?php
                  }
                }
                ?>
            </div>
            <div class="buy padded boxed top">
                <h2 class="section-h2 kuno">Write a comment</h2>
                <form method="post">
                    <input type="hidden" name="action" value="new_comment" readonly>
                    <input type="hidden" name="validation" value="<?= $form_validation ?>" readonly>
                    <div class="six"><input class="fill" type="text" name="txid" value="<?= sanitize_alphanumeric($_POST['txid']) ?>" placeholder="TXID"></div>
                    <div class="six"><input class="fill" type="text" name="tx_key" value="<?= sanitize_alphanumeric($_POST['tx_key']) ?>" placeholder="TX Key (Example: 7d7a9370a746d219e5f66aaaec902ead0d1c1783817a10276c9accdacca6f10d)"></div>
                    <div class="six"><input class="fill" type="text" name="email" value="<?= sanitize_utf8($_POST['email']) ?>" placeholder="Email (optional, for fundraiser rewards)"></div>
                    <div class="six no-top"><textarea class="fill" rows="2" maxlength="800" name="comment" placeholder="Write a public comment"><?= sanitize_utf8($_POST['comment'],800) ?></textarea>
                    </div>
                    <input type="submit" class="six btn green-button kuno" value="Comment">
                </form>
            </div>
            <div class="buy padded boxed top">
                <h2 class="section-h2 kuno">Share</h2>
                  <p class="top">Copy the link to share this fundraiser with friends or social media:</p>
                  <div class="six bottom"><input class="fill product-share kuno" type="text" name="price" readonly value="https://kuno.bitejo.com/fundraiser/<?= $fundraiser[0]['guid'] ?>-<?= sanitize_slug($fundraiser[0]['title']) ?>/"></div>
            </div>
        </div>
    </main>
    <div style="clear:both;"></div>
<?php display_footer(); ?>
