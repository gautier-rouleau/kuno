<?php
  require_once '/var/www/src/func/header.php';
  require_once '/var/www/src/db/fundraisers.php';
  require_once '/var/www/src/func/fundraisers.php';
  require_once '/var/www/src/func/sanitize.php';
  
  if(($_GET['view'] == 'categories') && (!$_POST)) {
    $get_categories = db_select_top_tags(200);
  } else {
    $terms = ($_POST['terms']) ? sanitize_utf8($_POST['terms']) : sanitize_utf8($_GET['terms']);
    $order_by = ($_POST['order_by']) ? sanitize_utf8($_POST['order_by']) : sanitize_utf8($_GET['order_by']);
    $terms = ($terms) ? $terms : 'all';
    $order_by = (in_array($order_by, array('date','last_donation','percentage','goal'))) ? $order_by : 'new';
    $fundraisers = db_search_fundraisers($terms,$order_by,$offset=0,$count=100);
  }
?>
<?php display_header($custom_title='Search | Kuno – Fundraise with Monero'); ?>
    <main id="content" class="group" role="main">
        <div class="main four centered">
          <div class="six padded no-bottom">
            <h2 class="section-h2 kuno">Search fundraisers</h2>
            <div class="new-fundraiser-box kuno six boxed padded top center new-fundraiser-nav kuno bottom">
              <form id="header_searchbar" method="post">
                <input class="inline icon icon-search header-search-input kuno search-page-input" type="text" name="terms" value="<?= $terms ?>" placeholder="Search fundraisers...">
                <select class="search-page-select" name="order_by">
                  <option <?= ($order_by == 'new') ? 'selected' : '' ?> value="date">Sort by new</option>
                  <option <?= ($order_by == 'last_donation') ? 'selected' : '' ?> value="last_donation">Sort by recently donated</option>
                  <option <?= ($order_by == 'percentage') ? 'selected' : '' ?> value="raised">Sort by percentage funded</option>
                  <option <?= ($order_by == 'goal') ? 'selected' : '' ?> value="goal">Sort by goal</option>
                </select>
                <button class="inline header-search-button kuno" type="submit"><img alt="Search" class="header-search-icon" src="https://kuno.bitejo.com/assets/search.png"></button>
               </form>
            </div>
            </div>
            <?php if(($_GET['view'] == 'categories') && (!$_POST)) { ?>
            <div class="padded">
              <div class="six boxed padded">
                <h3 class="top bottom">All Categories <span class="tag small dark"><?= count($get_categories) ?></span></h3>
                <?php
                  foreach($get_categories as $category) {
                ?>
                <div class="two <?= ($category['total'] > 1) ? 'bold' : 'faded no-bold' ?>">
                  <span class="gray faded">●</span> <a href="https://kuno.bitejo.com/search/<?= sanitize_slug($category['tags']) ?>/"><?= ucwords($category['tags']) ?> <span class="tag small dark"><?= $category['total'] ?></span></a>
                </div>
                <?php
                }
                ?>
              </div>
            </div>
            <?php } else { ?>
            <div class="six center homepage-gallery search-page no-top">
                   <?php
                      $fundraisers = array_slice($fundraisers, 0, 100);
                      if(!$fundraisers) {
                        echo '<p class="top padded"><i>No fundraisers found</i></p>';
                      }
                      foreach($fundraisers as $fundraiser) {
                    ?>
                    <a href="https://kuno.bitejo.com/<?= ($fundraiser['goal']) ? 'fundraiser' : 'donate' ?>/<?= $fundraiser['guid'] ?>/">
                        <div class="boxed two center valign-top homepage-product kuno">
                            <div class="homepage-product-img" style="background-image:url(<?= ($fundraiser['photo'] != 'none') ? $fundraiser['photo'] : 'https://kuno.bitejo.com/assets/placeholder.jpg' ?>)"></div>
                            <div class="homepage-product-title"> <?= $fundraiser['title'] ?></div>
                            <div class="homepage-product-progress kuno" style="background: linear-gradient(to right, #8fd476 <?= min(99, ceil($fundraiser['percentage'] * 100)) ?>%, #eee <?= min(99, ceil($fundraiser['percentage'] * 100)) ?>%, #eee 100%);">
                              <?= round($fundraiser['total'],4) ?> XMR<?= ($fundraiser['goal']) ? ' of '.round($fundraiser['goal'],4).' XMR' : '' ?>
                            </div>
                            <div class="homepage-product-price kuno">
                              <div class="three left no-padding homepage-product-support kuno"><?= (int) $fundraiser['supporters'] ?> supporters</div>
                              <div class="three right no-padding homepage-product-support kuno"><button class="homepage-product-button kuno">Support</button></div>
                            </div>
                        </div>
                    </a>
                    <?php } ?>
                  </div>
                  <?php } ?>
              </div>
            </div>
        </div>
    </main>
    <div style="clear:both;"></div>
<?php display_footer(); ?>
