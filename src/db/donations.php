<?php

function db_insert_donation($txid, $recipient, $amount, $blockheight, $tx_key, $comment, $email, $date) {
  global $db;
  try {
        $stmt = $db->prepare("INSERT INTO donations (txid, recipient, amount, blockheight, tx_key, comment, email, date) VALUES (?,?,?,?,?,?,?,?)");
        $stmt->execute(array($txid, $recipient, $amount, $blockheight, $tx_key, $comment, $email, $date));
        $affected_rows = $stmt->rowCount();
        $id = $db->lastInsertId();
        $result = array('status'=>'success','data'=>$id);
      } catch (PDOException $e) {
        $result = array('status'=>'error','data'=>$e);
      }
  return $result;
}

function db_select_donation_by_txid($txid) {
  global $db;
  $stmt = $db->prepare("SELECT * FROM donations WHERE txid = ?");
  $stmt->execute(array($txid));
  $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
  return $rows;
}

function db_count_donation_by_txid($txid) {
  global $db;
  $stmt = $db->prepare("SELECT COUNT(*) AS total FROM donations WHERE txid = ?");
  $stmt->execute(array($txid));
  $data = $stmt->fetchObject();
  $result = $data->total;
  return $result;
}

function db_select_fundraiser_donations($recipient) {
  global $db;
  $stmt = $db->prepare("SELECT * FROM donations WHERE recipient = ? ORDER BY blockheight DESC");
  $stmt->execute(array($recipient));
  $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
  return $rows;
}

function db_select_fundraiser_comments($recipient) {
  global $db;
  $stmt = $db->prepare("SELECT * FROM donations WHERE recipient = ? AND comment != '' ORDER BY blockheight DESC");
  $stmt->execute(array($recipient));
  $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
  return $rows;
}

function db_select_fundraiser_email_addresses($recipient) {
  global $db;
  $stmt = $db->prepare("SELECT * FROM donations WHERE recipient = ? AND email != '' ORDER BY blockheight DESC");
  $stmt->execute(array($recipient));
  $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
  return $rows;
}

function db_count_fundraiser_donations($recipient) {
  global $db;
  $stmt = $db->prepare("SELECT SUM(amount) AS total FROM donations WHERE recipient = ? GROUP BY recipient");
  $stmt->execute(array($recipient));
  $data = $stmt->fetchObject();
  $result = $data->total;
  return $result;
}

function db_count_fundraiser_supporters($recipient) {
  global $db;
  $stmt = $db->prepare("SELECT COUNT(id) AS total FROM donations WHERE recipient = ? GROUP BY recipient");
  $stmt->execute(array($recipient));
  $data = $stmt->fetchObject();
  $result = $data->total;
  return $result;
}

function db_count_fundraiser_comments($recipient) {
  global $db;
  $stmt = $db->prepare("SELECT COUNT(*) AS total FROM donations WHERE recipient = ? AND comment != ''");
  $stmt->execute(array($recipient));
  $data = $stmt->fetchObject();
  $result = $data->total;
  return $result;
}

function db_select_all_donations() {
  global $db;
  $stmt = $db->query('SELECT * FROM donations ORDER BY id DESC');
  $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
  return $rows;
}

function db_select_all_donations_limit($offset,$count) {
  global $db;
  $stmt = $db->prepare('SELECT * FROM donations ORDER BY id DESC LIMIT :count OFFSET :offset');
  $stmt->bindValue(':count', (int) $count, PDO::PARAM_INT);
  $stmt->bindValue(':offset', (int) $offset, PDO::PARAM_INT);
  $stmt->execute();
  $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
  return $rows;
}

function db_count_all_donations() {
  global $db;
  $stmt = $db->query("SELECT COUNT(*) AS `total` FROM donations");
  $data = $stmt->fetchObject();
  $result = $data->total;
  return $result;
}

function db_total_xmr_raised() {
  global $db;
  $stmt = $db->query("SELECT SUM(amount) AS `total` FROM donations");
  $data = $stmt->fetchObject();
  $result = $data->total;
  return $result;
}

function db_update_donation($txid,$change,$value) {
  global $db;
  if($change == 'tx_key') {
    $query = "UPDATE donations SET tx_key=? WHERE txid=?";
  } else if ($change == 'comment') {
    $query = "UPDATE donations SET comment=? WHERE txid=?";
  } else if ($change == 'email') {
    $query = "UPDATE donations SET email=? WHERE txid=?";
  } else if ($change == 'status') {
    $query = "UPDATE donations SET status=? WHERE txid=?";
  } else {
    return false;
  }
  try {
    $stmt = $db->prepare($query);
    $stmt->execute(array($value, $txid));
    $affected_rows = $stmt->rowCount();
    $result = array('status'=>'success','data'=>$affected_rows);
  } catch (PDOException $e) {
    $result = array('status'=>'error','data'=>$e);
  }
  return $result;
}

function db_delete_donation($txid) {
  global $db;
  try {
    $stmt = $db->prepare("DELETE FROM donations WHERE txid=?");
    $stmt->execute(array($txid));
    $affected_rows = $stmt->rowCount();
    $result = array('status'=>'success','data'=>$affected_rows);
  } catch (PDOException $e) {
    $result = array('status'=>'error','data'=>$e);
  }
  return $result;
}

?>