<?php

function db_insert_fundraiser($guid, $address, $view_key, $goal, $title, $description, $tags, $photo, $secondary_photos, $email, $password, $status, $date, $last_synced) {
  global $db;
  try {
        $stmt = $db->prepare("INSERT INTO fundraisers (guid, address, view_key, goal, title, description, tags, photo, secondary_photos, email, password, status, date, last_synced) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
        $stmt->execute(array($guid, $address, $view_key, $goal, $title, $description, $tags, $photo, $secondary_photos, $email, $password, $status, $date, $last_synced));
        $affected_rows = $stmt->rowCount();
        $id = $db->lastInsertId();
        $result = array('status'=>'success','data'=>$id);
      } catch (PDOException $e) {
        $result = array('status'=>'error','data'=>$e);
      }
  return $result;
}

function db_select_fundraiser_by_guid($guid) {
  global $db;
  $stmt = $db->prepare("SELECT * FROM fundraisers WHERE guid = ?");
  $stmt->execute(array($guid));
  $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
  return $rows;
}

function db_count_fundraiser_by_guid($guid) {
  global $db;
  $stmt = $db->prepare("SELECT COUNT(*) AS total FROM fundraisers WHERE guid = ?");
  $stmt->execute(array($guid));
  $data = $stmt->fetchObject();
  $result = $data->total;
  return $result;
}

function db_select_fundraiser_by_address($address) {
  global $db;
  $stmt = $db->prepare("SELECT * FROM fundraisers WHERE address = ?");
  $stmt->execute(array($address));
  $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
  return $rows;
}

function db_count_fundraiser_by_address($address) {
  global $db;
  $stmt = $db->prepare("SELECT COUNT(*) AS total FROM fundraisers WHERE address = ?");
  $stmt->execute(array($address));
  $data = $stmt->fetchObject();
  $result = $data->total;
  return $result;
}

function db_select_fundraisers_by_recent_donation() {
  global $db;
  $stmt = $db->query('SELECT f.guid AS guid, f.title AS title, f.photo AS photo, f.goal AS goal, f.status AS status, f.date AS date, SUM(d.amount) AS total, MAX(d.blockheight) as last_donation, (SUM(d.amount)/(f.goal+0.01)) AS percentage FROM fundraisers AS f LEFT JOIN donations AS d ON f.address = d.recipient WHERE status = "active" GROUP BY f.guid ORDER BY last_donation DESC LIMIT 10');
  $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
  return $rows;
}

function db_select_random_fundraisers() {
  global $db;
  $stmt = $db->query('SELECT f.guid AS guid, f.title AS title, f.photo AS photo, f.goal AS goal, f.status AS status, f.date AS date, SUM(d.amount) AS total, COUNT(d.txid) AS supporters, MAX(d.blockheight) as last_donation, (SUM(d.amount)/(f.goal+0.01)) AS percentage FROM fundraisers AS f LEFT JOIN donations AS d ON f.address = d.recipient WHERE status = "active" GROUP BY f.guid ORDER BY RAND() LIMIT 20');
  $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
  return $rows;
}

function db_select_random_fundraisers_with_photo() {
  global $db;
  $stmt = $db->query('SELECT f.guid AS guid, f.title AS title, f.photo AS photo, f.goal AS goal, f.status AS status, f.date AS date, SUM(d.amount) AS total, COUNT(d.txid) AS supporters, MAX(d.blockheight) as last_donation, (SUM(d.amount)/(f.goal+0.01)) AS percentage FROM fundraisers AS f LEFT JOIN donations AS d ON f.address = d.recipient WHERE f.photo != "none" AND status = "active" GROUP BY f.guid ORDER BY RAND() LIMIT 20');
  $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
  return $rows;
}

function db_select_top_tags($limit=20) {
  global $db;
  $stmt = $db->query('SELECT tags FROM fundraisers WHERE status = "active"');
  $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
  $tags_list = implode(array_column($rows, 'tags'),',');
  $tags = explode(',',$tags_list);
  $result = array();
  foreach($tags as $tag) {
    $tag = trim($tag);
    if($result[$tag]['tags']) {
      $result[$tag]['total'] = (int) $result[$tag]['total'] + 1;
    } else {
      $result[$tag]['tags'] = $tag;
      $result[$tag]['total'] = 1;
    }
  }
  usort($result, function($a, $b) {
    return $b['total'] - $a['total'];
  });
  $result = array_slice($result, 0, $limit);
  return $result;
}

function db_search_fundraisers($terms='all',$order_by='new',$offset=0,$count=20) {
  global $db;
  $order_by = 'f.date DESC';
  if($order == 'goal') {
    $order_by = 'f.goal DESC';
  } else if($order == 'last_donation') {
    $order_by = 'last_donation DESC';
  } else if($order == 'percentage') {
    $order_by = 'percentage DESC';
  }
  if($terms != 'all') {
    $where = 'WHERE status = "active" AND LOWER(f.title) LIKE CONCAT("%", ?, "%") OR LOWER(f.tags) LIKE CONCAT("%", ?, "%") OR LOWER(f.description) LIKE CONCAT("%", ?, "%")';
  } else {
    $where = 'WHERE status = "active"';
  }
  $stmt = $db->prepare('SELECT f.guid AS guid, f.title AS title, f.photo AS photo, f.goal AS goal, f.description AS description, f.status AS status, f.date AS date, SUM(d.amount) AS total, COUNT(d.txid) AS supporters, MAX(d.blockheight) as last_donation, (SUM(d.amount)/(f.goal+0.01)) AS percentage
                        FROM fundraisers AS f
                        LEFT JOIN donations AS d ON f.address = d.recipient
                        '.$where.'
                        GROUP BY f.guid
                        ORDER BY '.$order_by.' LIMIT ? OFFSET ?');
  if($terms != 'all') {
    $stmt->bindValue(1, $terms);
    $stmt->bindValue(2, $terms);
    $stmt->bindValue(3, $terms);
    $stmt->bindValue(4, (int) $count, PDO::PARAM_INT);
    $stmt->bindValue(5, (int) $offset, PDO::PARAM_INT);
  } else {
    $stmt->bindValue(1, (int) $count, PDO::PARAM_INT);
    $stmt->bindValue(2, (int) $offset, PDO::PARAM_INT);
  }
  $stmt->execute();
  $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
  return $rows;
}

function db_select_all_fundraisers() {
  global $db;
  $stmt = $db->query('SELECT * FROM fundraisers ORDER BY id DESC');
  $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
  return $rows;
}

function db_select_all_fundraiser_addresses() {
  global $db;
  $stmt = $db->query('SELECT guid, address, view_key FROM fundraisers ORDER BY last_synced ASC');
  $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
  return $rows;
}

function db_select_all_fundraisers_limit($offset,$count) {
  global $db;
  $stmt = $db->prepare('SELECT * FROM fundraisers ORDER BY id DESC LIMIT :count OFFSET :offset');
  $stmt->bindValue(':count', (int) $count, PDO::PARAM_INT);
  $stmt->bindValue(':offset', (int) $offset, PDO::PARAM_INT);
  $stmt->execute();
  $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
  return $rows;
}

function db_count_all_fundraisers() {
  global $db;
  $stmt = $db->query("SELECT COUNT(*) AS `total` FROM fundraisers");
  $data = $stmt->fetchObject();
  $result = $data->total;
  return $result;
}

function db_update_fundraiser($guid,$change,$value) {
  global $db;
  if($change == 'title') {
    $query = "UPDATE fundraisers SET title=? WHERE guid=?";
  } else if ($change == 'description') {
    $query = "UPDATE fundraisers SET description=? WHERE guid=?";
  } else if ($change == 'tags') {
    $query = "UPDATE fundraisers SET tags=? WHERE guid=?";
  } else if ($change == 'photo') {
    $query = "UPDATE fundraisers SET photo=? WHERE guid=?";
  } else if ($change == 'secondary_photos') {
    $query = "UPDATE fundraisers SET secondary_photos=? WHERE guid=?";
  } else if ($change == 'goal') {
    $query = "UPDATE fundraisers SET goal=? WHERE guid=?";
  } else if ($change == 'address') {
    $query = "UPDATE fundraisers SET address=? WHERE guid=?";
  } else if ($change == 'view_key') {
    $query = "UPDATE fundraisers  SET view_key=? WHERE guid=?";
  } else if ($change == 'email') {
    $query = "UPDATE fundraisers  SET email=? WHERE guid=?";
  } else if ($change == 'password') {
    $query = "UPDATE fundraisers  SET password=? WHERE guid=?";
  } else if ($change == 'last_synced') {
    $query = "UPDATE fundraisers SET last_synced=? WHERE guid=?";
  } else if ($change == 'status') {
    $query = "UPDATE fundraisers SET status=? WHERE guid=?";
  } else {
    return false;
  }
  try {
    $stmt = $db->prepare($query);
    $stmt->execute(array($value, $guid));
    $affected_rows = $stmt->rowCount();
    $result = array('status'=>'success','data'=>$affected_rows);
  } catch (PDOException $e) {
    $result = array('status'=>'error','data'=>$e);
  }
  return $result;
}

function db_delete_fundraiser($guid) {
  global $db;
  try {
    $stmt = $db->prepare("DELETE FROM fundraisers WHERE guid=?");
    $stmt->execute(array($guid));
    $affected_rows = $stmt->rowCount();
    $result = array('status'=>'success','data'=>$affected_rows);
  } catch (PDOException $e) {
    $result = array('status'=>'error','data'=>$e);
  }
  return $result;
}

?>