SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";
CREATE DATABASE IF NOT EXISTS `kuno_db` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `kuno_db`;

CREATE TABLE `confirmation_codes` (
  `id` int NOT NULL,
  `fundraiser_guid` varchar(255) NOT NULL,
  `action` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `date` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `donations` (
  `id` int NOT NULL,
  `txid` varchar(255) NOT NULL,
  `recipient` varchar(255) NOT NULL,
  `amount` float NOT NULL,
  `blockheight` int NOT NULL,
  `tx_key` varchar(255) NOT NULL,
  `comment` text NOT NULL,
  `email` varchar(255) NOT NULL,
  `date` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `fundraisers` (
  `id` int NOT NULL,
  `guid` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `view_key` varchar(255) NOT NULL,
  `goal` float NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `tags` varchar(255) NOT NULL,
  `photo` varchar(255) NOT NULL,
  `secondary_photos` text NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `date` int NOT NULL,
  `last_synced` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


ALTER TABLE `confirmation_codes`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `donations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `txid` (`txid`),
  ADD KEY `recipient` (`recipient`);

ALTER TABLE `fundraisers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `guid` (`guid`),
  ADD KEY `address` (`address`),
  ADD KEY `status` (`status`),
  ADD KEY `last_synced` (`last_synced`);

ALTER TABLE `confirmation_codes`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

ALTER TABLE `donations`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

ALTER TABLE `fundraisers`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;