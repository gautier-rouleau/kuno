<?php
date_default_timezone_set('UTC');

try {
  $db = new PDO('mysql:host=localhost;dbname=kuno_db;charset=utf8', 'kuno_user', 'YOUR_PASSWORD_HERE');
} catch(PDOException $ex) {
  die("Could not connect to database");
}
    
$sql_status = 1;
?>
