<?php

function db_insert_confirmation_code($fundraiser_guid, $action, $code, $status, $date) {
  global $db;
  try {
        $stmt = $db->prepare("INSERT INTO confirmation_codes (fundraiser_guid, action, code, status, date) VALUES (?,?,?,?,?)");
        $stmt->execute(array($fundraiser_guid, $action, $code, $status, $date));
        $affected_rows = $stmt->rowCount();
        $id = $db->lastInsertId();
        $result = array('status'=>'success','data'=>$id);
      } catch (PDOException $e) {
        $result = array('status'=>'error','data'=>$e);
      }
  return $result;
}

function db_select_confirmation_code_by_id($id) {
  global $db;
  $stmt = $db->prepare("SELECT * FROM confirmation_codes WHERE id=?");
  $stmt->execute(array($id));
  $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
  return $rows;
}

function db_select_confirmation_code_by_code($code) {
  global $db;
  $stmt = $db->prepare("SELECT * FROM confirmation_codes WHERE code=?");
  $stmt->execute(array($code));
  $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
  return $rows;
}

function db_select_confirmation_code_by_fundraiser($fundraiser_guid) {
  global $db;
  $stmt = $db->prepare("SELECT * FROM confirmation_codes WHERE fundraiser_guid=?");
  $stmt->execute(array($fundraiser_guid));
  $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
  return $rows;
}

function db_select_valid_confirmation_code($fundraiser_guid,$action,$code,$date) {
  global $db;
  $stmt = $db->prepare("SELECT * FROM confirmation_codes WHERE fundraiser_guid=? AND action=? AND code=? AND date > ? AND status='active' ORDER BY date DESC LIMIT 1");
  $stmt->execute(array($fundraiser_guid,$action,$code,$date));
  $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
  return $rows;
}

function db_update_confirmation_code($id,$change,$value) {
  global $db;
  if ($change == 'status') {
    $query = "UPDATE confirmation_codes SET status=? WHERE id=?";
  } else if ($change == 'expire_existing') {
    $value = 'expired';
    $query = "UPDATE confirmation_codes SET status=? WHERE fundraiser_guid=?";
  } else {
    return 1;
  }
  try {
    $stmt = $db->prepare($query);
    $stmt->execute(array($value, $id));
    $affected_rows = $stmt->rowCount();
    $result = array('status'=>'success','data'=>$affected_rows);
  } catch (PDOException $e) {
    $result = array('status'=>'error','data'=>$e);
  }
  return $result;
}

function db_delete_confirmation_code($code) {
  global $db;
  $stmt = $db->prepare("DELETE FROM confirmation_codes WHERE code=?");
  $stmt->execute(array($code));
  $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
  return $rows;
}

?>