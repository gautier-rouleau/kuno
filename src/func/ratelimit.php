<?php
require_once '/var/www/src/func/generate_guid.php';
require_once '/var/www/src/func/sanitize.php';
require_once '/var/www/src/func/encrypt.php';

function ratelimit_tokens($update_tokens=false) {
  if(!isset($_SESSION['ratelimit_bucket'])) {
    $default_tokens = 500;
    $_SESSION['ratelimit_bucket'] = $default_tokens;
    $_SESSION['ratelimit_last_updated'] = time();
    return $default_tokens;
  } else {
    $ratelimit_last_updated = $_SESSION['ratelimit_last_updated'];
    $current_time = time();
    $elapsed_seconds = $current_time - $ratelimit_last_updated;
    $_SESSION['ratelimit_bucket'] = (int) $_SESSION['ratelimit_bucket'] + (int) $elapsed_seconds;
    if($update_tokens) {
      $_SESSION['ratelimit_bucket'] = (int) $update_tokens;
    }
    $_SESSION['ratelimit_bucket'] = ((int) $update_tokens > 1000) ? 1000 : $_SESSION['ratelimit_bucket'];
    $_SESSION['ratelimit_last_updated'] = time();
    $tokens_count = (int) $_SESSION['ratelimit_bucket'];
    return $tokens_count;
  }
}

function init_ratelimit($check_guid='guid') {
  $salt = generate_hash();
  $validation_string = $salt.'|'.$check_guid.'|'.time();
  $validation = encrypt_string($validation_string);
  if(!$_SESSION['validation']) {
    $_SESSION['validation'] = array();
    $_SESSION['validation'][] = $validation; 
  } else {
    $count_validations = count($_SESSION['validation']);
    if($count_validations >= 10) {
      $trim_validations = $count_validations - 8;
      array_slice($_SESSION['validation'],0,$trim_validations);
    }
    $_SESSION['validation'][] = $validation;
  }
  return $validation;
}

function check_ratelimit($post_validation, $check_guid, $tokens_cost) {
  $tokens_count = ratelimit_tokens();
  if((int) $tokens_count >= (int) $tokens_cost) {
    $remaining_tokens = (int) $tokens_count - (int) $tokens_cost;
    ratelimit_tokens($remaining_tokens);
  } else {
    $tokens_count = (int) $tokens_count - 5;
    ratelimit_tokens($tokens_count);
    $required_tokens = (int) $tokens_cost - (int) $tokens_count;
    $required_time = ($required_tokens > 60) ? ceil((float) $required_tokens / 60).' minutes' : $required_tokens.' seconds';
    return array('status'=>'error', 'data'=>'Ratelimited: Please wait '.$required_time.' and try again');
  }
  $validation = sanitize_utf8(decrypt_string($post_validation));
  $validation = explode('|',$validation);
  $val_salt = $validation[0];
  $val_guid = $validation[1];
  $val_loadtime = $validation[2];
  $current_time = time();
  if(!in_array($post_validation,$_SESSION['validation'])) {
    return array('status'=>'error', 'data'=>'Form expired, please refresh');
  } else {
    $array_key = array_search($post_validation,$_SESSION['validation']);
    if($array_key !== NULL) {
      unset($_SESSION['validation'][$array_key]);
      array_values($_SESSION['validation']);
    }
  }
  if($val_loadtime < ($current_time - (60 * 60))) {
    return array('status'=>'error', 'data'=>'Form expired, please refresh');
  } else if($val_loadtime > ($current_time - 2)) {
    return array('status'=>'error', 'data'=>'[Ratelimited] Please wait 2 seconds and try again');
  }
  if($val_guid != $check_guid) {
    return array('status'=>'error', 'data'=>'Session expired, please refresh');
  }
  return array('status'=>'success', 'data'=>'Form successfully submitted');
}

?>