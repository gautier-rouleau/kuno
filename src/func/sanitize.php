<?php

function sanitize_alphanumeric($str,$length=255) {
  $str = trim(strip_tags($str));
  $str = preg_replace('/[^A-Za-z0-9]/', '', $str);
  $str = substr($str,0,$length);
  return $str;
}

function sanitize_hex($str,$length=255) {
  $str = strtolower(trim(strip_tags($str)));
  $str = preg_replace('/[^a-f0-9]/', '', $str);
  $str = substr($str,0,$length);
  return $str;
}

function sanitize_ascii($str) {
  $str = trim(strip_tags($str));
  $asciis = 'a-zA-Z0-9\s~!@#$%^&*()_+-={}|:;<>?,.\/\"\'\\\[\]';
  $str = preg_replace("/[^$asciis]/", '', $str);
  return $str;
}

function sanitize_password($str) {
  $str = str_replace(chr(0), '', $str);
  $str = trim(strip_tags($str));
  $str = substr($str,0,255);
  return $str;
}

function sanitize_alphanumeric_extended($str,$length=255) {
  $str = trim(strip_tags($str));
  $str = preg_replace('/[^A-Za-z0-9:_\-\.\ ]/', '', $str);
  $str = substr($str,0,$length);
  return $str;
}

function sanitize_slug($str,$length=255) {
  $str = trim(strip_tags($str));
  $str = preg_replace('/[^A-Za-z0-9\-\ ]/', '', $str);
  $str = str_replace(' ', '-', $str);
  $str = substr($str,0,$length);
  $str = strtolower($str);
  return $str;
}

function sanitize_email($str,$length=255) {
  $str = trim(strip_tags($str));
  $str = preg_replace('/[^A-Za-z0-9_@\%\+\-\.]/', '', $str);
  $str = substr($str,0,$length);
  return $str;
}

function sanitize_utf8($str,$length=255) {
  $str = str_replace(chr(0), '', $str);
  $str = preg_replace('/[^\P{C}\r\n\s]+/u', '', $str);
  $str = trim(strip_tags($str));
  $str = str_replace(array('javascript:'),'',$str);
  $str = substr($str,0,$length);
  $str = htmlentities($str, ENT_QUOTES, "UTF-8", false);
  return $str;
}

function base64_urlencode($str,$only_urlencode=false) {
  $str = (!$only_urlencode) ? base64_encode($str) : $str;
  $str = str_replace('/','_',$str);
  $str = str_replace('+','-',$str);
  $str = str_replace('=','',$str);
  return $str;
}

function base64_urldecode($str,$only_urldecode=false) {
  $str_len = strlen($str);
  if(($str_len % 4) == 2) {
    $str = $str.'==';
  } else if(($str_len % 4) == 3) {
    $str = $str.'=';
  }
  $str = str_replace('_','/',$str);
  $str = str_replace('-','+',$str);
  $str = (!$only_urldecode) ? base64_decode($str) : $str;
  return $str;
}

?>