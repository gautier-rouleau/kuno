<?php
require_once '/var/www/src/func/session.php';
function display_header($custom_title=false, $custom_description=false, $custom_photo=false) {
  if($_GET['switch_mode'] == 'dark') {
    $_SESSION['dark_mode'] = 'dark';
  } else if($_GET['switch_mode'] == 'light') {
    unset($_SESSION['dark_mode']);
  }
  $title = ($custom_title) ? $custom_title : 'Kuno – Fundraise with Monero';
  $description = ($custom_description) ? $custom_description : 'Raise money or donate to a good cause with Monero. Peer-to-peer, KYC-free and easy-to-use.';
  $photo = ($custom_photo) ? $custom_photo : 'https://kuno.bitejo.com/assets/banner.jpg';
?>
<html lang="en"><head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= $title ?></title>
    <meta name="robots" content="index, follow">
    <meta name="keywords" content="monero, xmr, fundraisers, crowdfunding, gofundme, kickstarter, patreon, buymeacoffee, ko-fi, charity, startup, crypto, cryptocurrency, kyc-free, no kyc">
    <meta name="description" content="<?= $description ?>">
    <meta itemprop="name" content="<?= $title ?>">
    <meta itemprop="description" content="<?= $description ?>">
    <meta itemprop="image" content="<?= $photo ?>">
    <meta property="og:url" content="https://kuno.bitejo.com/">
    <meta property="og:type" content="website">
    <meta property="og:title" content="<?= $title ?>">
    <meta property="og:description" content="<?= $description ?>">
    <meta property="og:image" content="<?= $photo ?>">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="<?= $title ?>">
    <meta name="twitter:description" content="<?= $description ?>">
    <meta name="twitter:image" content="<?= $photo ?>">
    <link rel="stylesheet" href="https://kuno.bitejo.com/assets/style.css">
    <?php if($_SESSION['dark_mode']) { ?>
      <style>
        html {
          filter: invert(1);
        }
        .homepage-gallery .homepage-product-img, .card.product .product-img, .product-holder, .product-additional-photos img, .use-case-image, img {
          filter: brightness(0.6) invert(1);
        }
        .homepage-top-cta {
          background: #fff !important;
          border-top: 2px solid #ff7a00;
          border-bottom: 2px solid #ff7a00;
        }
        .homepage-top-cta p.white {
          color: #000 !important;
        }
        .homepage-cta {
          background: #fff !important;
          color: #000 !important;
          border-top: 4px solid #ff7a00;
          border-bottom: 4px solid #ff7a00;
        }
      </style>
    <?php } ?>
    <link rel="shortcut icon" href="https://kuno.bitejo.com/assets/monero_icon2.png">
</head>

<body>
    <header class="kuno">
        <div class="three header-logo">
            <a href="https://kuno.bitejo.com">
                <div class="inline p-left p-right"><img src="https://kuno.bitejo.com/assets/monero_heart_icon.png" class="logo-small" alt="">
                    <h1 class="header-name kuno"> Kuno</h1>
                </div>
            </a>
        </div>
        <div class="one-half header-searchbar header-searchbar-left kuno right">
          <a href="https://kuno.bitejo.com/new-fundraiser/"><button class="header-button kuno">New fundraiser</button></a>
        </div>
        <div class="one-half header-searchbar header-searchbar-right kuno right">
            <form id="header_searchbar" method="post" action="https://kuno.bitejo.com/search/">
            <input class="inline icon icon-search header-search-input kuno" type="text" name="terms" placeholder="Search fundraisers...">
            <button class="inline header-search-button kuno" type="submit"><img alt="Search" class="header-search-icon" src="https://kuno.bitejo.com/assets/search.png"></button>
            </form>
        </div>
    </header>
<?php
}

function display_footer() {
?>
<footer class="kuno-footer">
        <div class="two footer-box">
            <div class="heading bottom">Kuno</div>
            <p>Fundraise with Monero</p>
            <p>Contact: bitejo [at] bitejo.com</p>
            <p class="top">
              <?php if($_SESSION['dark_mode']) { ?>
              <a class="small inline" href="https://kuno.bitejo.com/light-mode/">Light Mode</a>
              <?php } else { ?>
              <a class="small inline" href="https://kuno.bitejo.com/dark-mode/">Dark Mode</a>
              <?php } ?>
            </p>
        </div>
        <div class="two footer-links">
            <p>Manage your fundraiser:</p>
            <ul>
                <li><a href="https://kuno.bitejo.com/new-fundraiser/">New Fundraiser</a></li>
                <li><a href="https://kuno.bitejo.com/edit-fundraiser/">Edit Fundraiser</a></li>
                <li><a href="https://kuno.bitejo.com/reset-password/">Reset Password</a></li>
            </ul>
        </div>
        <div class="two footer-links">
            <p>Quick Links:</p>
            <ul>
                <li><a href="https://kuno.bitejo.com/search/">Search Fundraisers</a></li>
                <li><a href="https://kuno.bitejo.com/search/categories/">View Categories</a></li>
                <li><a href="https://kuno.bitejo.com/rss/">RSS Feed</a></li>
            </ul>
        </div>
    </footer>
</body>
</html>
<?php
}
?>