<?php
function convert_image($img,$width,$height,$avatar=false,$thumbnail=false) {
  if($avatar) {
    $target_width = 100;
    $target_height = 100;
  } else {
    if($thumbnail) {
      $max_width = 200;
      $max_height = 200;
    } else {
      $max_width = 800;
      $max_height = 800;
    }
    if($width > $height) {
      $target_width = ($width < $max_width) ? $width : $max_width;
      $divisor = $width / $target_width;
      $target_height = floor($height / $divisor);
    } else {
      $target_height = ($height < $max_height) ? $height : $max_height;
      $divisor = $height / $target_height;
      $target_width = floor($width / $divisor);
    }
  }
  $target_layer = imagecreatetruecolor($target_width,$target_height);
  $white = imagecolorallocate($target_layer, 255, 255, 255); 
  imagefill($target_layer,0,0,$white); 
  imagecopyresampled($target_layer,$img,0,0,0,0,$target_width,$target_height,$width,$height);
  return $target_layer;
}

function upload_image($filename_default,$file_field=null,$avatar=false,$no_thumbnail=false) {
  $max_size = 2000000;
  $whitelist_ext = array('jpeg','jpg','png','JPEG','JPG','PNG');
  $whitelist_type = array('image/jpeg', 'image/jpg', 'image/png');
  if (!$file_field) {
    $out['error'] = "Image must be PNG or JPG";
    return $out;          
  }
  
  if((!empty($_FILES[$file_field])) && ($_FILES[$file_field]['error'] == 0)) {
    // Get filename
    $file_info = pathinfo($_FILES[$file_field]['name']);
    $name = $file_info['filename'];
    $ext = $file_info['extension']; 
    
    if (!in_array($ext, $whitelist_ext)) {
      $out['error'] = "Image must be PNG or JPG";
      return $out;
    }
    
    if (!in_array($_FILES[$file_field]["type"], $whitelist_type)) {
      $out['error'] = "Image must be PNG or JPG";
      return $out;
    } 
    
    if ($_FILES[$file_field]["size"] > $max_size) {
      $out['error'] = "Maximum image size is 2 MB";
      return $out;
    }
    
    if (!getimagesize($_FILES[$file_field]['tmp_name'])) {
      $out['error'] = "Invalid image";
      return $out;
    }

    if((float) disk_free_space('/') < 5000000000) {
      $out['error'] = "Storage full";
      return $out;
    }

    if($avatar) {
      $total_file_name = '/var/www/html/assets/uploads/'.hash('sha256', $filename_default).'.jpeg';
    } else {
      $total_file_name = '/var/www/html/assets/uploads/'.hash('sha256', $filename_default).'.jpeg';
      $total_file_name_thumb = '/var/www/html/assets/uploads/'.hash('sha256', $filename_default).'_thumb.jpeg';
    }
    
    $gd_file = $_FILES[$file_field]['tmp_name']; 
    $gd_source_properties = getimagesize($gd_file);
    $gd_ext = pathinfo($_FILES[$file_field]['name'], PATHINFO_EXTENSION);
    $gd_image_type = $gd_source_properties[2];

    switch ($gd_image_type) {
      case IMAGETYPE_PNG:
        $gd_img = imagecreatefrompng($gd_file);
        if(!$gd_img) {
          $out['error'] = "Image must be PNG or JPG";
          return $out;
        } else {
          imagealphablending($gd_img, true);
          imagesavealpha($gd_img, true);
        }
      break;
      case IMAGETYPE_JPEG:
        $gd_img = imagecreatefromjpeg($gd_file);
        if(!$gd_img) {
          $out['error'] = "Image must be PNG or JPG";
          return $out;
        } 
      break;
      default:
        $out['error'] = "Image must be PNG or JPG";
        return $out;
      break;
    }

    $target_layer = convert_image($gd_img,$gd_source_properties[0],$gd_source_properties[1],$avatar);

    $saved_img = imagejpeg($target_layer,$total_file_name,90);
    if((!$avatar) && (!$no_thumbnail)) {
      $target_layer_thumb = convert_image($gd_img,$gd_source_properties[0],$gd_source_properties[1],false,true);
      $saved_img_thumb = imagejpeg($target_layer_thumb,$total_file_name_thumb,90);
    }
    if(!$saved_img) {
        $out['error'] = "Invalid image";
        return $out;
    } else {
        $out['success'] = "Image uploaded";
        return $out;
    }    
  } else {
    if($_FILES[$file_field]['error']) {
      $out['error'] = "Image must be PNG or JPG and smaller than 2 MB";
    } else {
      $out['error'] = "Please choose an image";
    }
    return $out;
  }  
}

function delete_uploaded_image($filename,$type,$key=false) {
  if($key != 'bi0HMROlLZy52I4IlJYmyi9GEOKh10g2') {
    return false;
  }
  $filename = trim(strip_tags($filename));
  $asciis = 'a-zA-Z0-9\s~!@#$%^&*()_+-={}|:;<>?,.\/\"\'\\\[\]';
  $filename = preg_replace("/[^$asciis]/", '', $filename);
  $full_filename = '/var/www/html/assets/uploads/'.$filename;
  unlink($full_filename);
  return true;
}
?>
