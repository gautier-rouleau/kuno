<?php
require_once '/var/www/src/db/connect.php';
require_once '/var/www/src/db/fundraisers.php';
require_once '/var/www/src/db/confirmation_codes.php';
require_once '/var/www/src/func/generate_guid.php';
require_once '/var/www/src/func/sanitize.php';

function send_confirmation_email($recipient,$subject,$message) {
  $recipient = filter_var($recipient, FILTER_SANITIZE_EMAIL);
  $subject = sanitize_utf8($subject,80);
  $message = wordwrap(sanitize_utf8($message,800),80);
  if(($recipient) && ($subject) && ($message)) {
    $headers = 'From: Kuno <noreply@bitejo.com>'."\r\n".
             'Reply-To: Kuno <noreply@bitejo.com>';
    $send_email = mail($recipient, $subject, $message, $headers, '-fnoreply@bitejo.com -FKuno -rnoreply@bitejo.com');
    return $send_email;
  } else {
    return false;
  }
}

function send_confirmation_code($fundraiser_guid,$email,$action) {
  $code = generate_confirmation_code();
  $date = time();
  $date_plus_24h = $date + (60 * 60 * 24);
  $status = 'active';
  db_update_confirmation_code($fundraiser_guid, 'expire_existing', 'expired');
  db_insert_confirmation_code($fundraiser_guid, $action, $code, $status, $date);
  $subject = '[Kuno Fundraisers] Reset password';
  $message = 'Visit this link to reset your password: https://kuno.bitejo.com/reset-password/'.urlencode($email).'/'.$fundraiser_guid.'/'.$code.'/ or enter this code: '.$code.' (Valid for 24 hours)';
  send_confirmation_email($email,$subject,$message);
}

function check_confirmation_code($fundraiser_guid,$action,$code) {
  $code = sanitize_utf8($code);
  $date = time();
  $date_minus_24h = $date - (60 * 60 * 24);
  $check_confirm = db_select_valid_confirmation_code($fundraiser_guid,$action,$code,$date_minus_24h);
  if(!$check_confirm) {
    $result = array('status'=>'error','data'=>'Confirmation code is invalid or expired.');
    return $result;
  } else {
    if($check_confirm[0]['fundraiser_guid'] != $fundraiser_guid) {
      $result = array('status'=>'error','data'=>'Confirmation code is invalid or expired.');
      return $result;
    }
    db_update_confirmation_code($check_confirm[0]['id'],'status','confirmed');
    $result = array('status'=>'success','data'=>'Code is valid');
    return $result;
  }
}

function reset_password($fundraiser_guid, $email, $confirmation_code=false, $new_password=false) {
  $fundraiser_guid = sanitize_utf8($fundraiser_guid);
  $email = sanitize_utf8($email);
  if((!$fundraiser_guid) || (!$email)) {
    $result = array('status'=>'error','data'=>'Please enter your fundraiser code and email address');
    return $result;
  }
  $fundraiser = db_select_fundraiser_by_guid($fundraiser_guid);
  if(count($fundraiser) < 1) {
    $result = array('status'=>'error','data'=>'Fundraiser not found');
    return $result;
  }
  if((!$fundraiser[0]['email']) || ($fundraiser[0]['email'] == 'none')) {
    $result = array('status'=>'error','data'=>'No email address is set, please contact support');
    return $result;
  }
  if($fundraiser[0]['email'] != $email) {
    $result = array('status'=>'error','data'=>'Incorrect email address');
    return $result;
  }
  if(!$confirmation_code) {
    send_confirmation_code($fundraiser[0]['guid'],$fundraiser[0]['email'],'reset_password');
    $result = array('status'=>'success','data'=>'Check your email address for a password reset code.');
    return $result;
  } else {
    $confirmation_code = sanitize_utf8($confirmation_code);
    $new_password = sanitize_password($new_password);
    if((!$confirmation_code) || (!$new_password)) {
      $result = array('status'=>'error','data'=>'Please enter your password reset code and new password');
      return $result;
    }
    $check_confirmation_code = check_confirmation_code($fundraiser[0]['guid'],'reset_password',$confirmation_code);
    if($check_confirmation_code['status'] != 'success') {
      $result = array('status'=>'error','data'=>$check_confirmation_code['data']);
      return $result;
    } else {
      $password_hash = password_hash($new_password, PASSWORD_DEFAULT);
      $update_password = db_update_fundraiser($fundraiser[0]['guid'],'password',$password_hash);
      $result = array('status'=>'success','data'=>'Password changed, you can now <a href="https://kuno.bitejo.com/edit-fundraiser/'.$fundraiser[0]['guid'].'">edit your fundraiser</a>.');
      return $result;
    }
  }
}

?>