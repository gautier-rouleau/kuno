<?php
function generate_guid() {
  $data = openssl_random_pseudo_bytes(16,$cstrong);
  $data[6] = chr(ord($data[6]) & 0x0f | 0x40);
  $data[8] = chr(ord($data[8]) & 0x3f | 0x80);
  return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
}

function generate_short_guid() {
  $code = openssl_random_pseudo_bytes(16,$cstrong);
  $code = base64_encode($code);
  $code = strtolower(str_replace(array('/','+','='),'',$code));
  return vsprintf('%s%s%s%s', str_split($code));
}

function generate_confirmation_code() {
  $code = openssl_random_pseudo_bytes(16,$cstrong);
  $code = base64_encode($code);
  $code = str_replace(array('/','+','=','I','l','1','0','O'),'',$code);
  return vsprintf('%s%s%s%s%s%s', str_split($code));
}

function generate_hash() {
  $string = hash('sha256',openssl_random_pseudo_bytes(16,$cstrong));
  return $string;
}
?>