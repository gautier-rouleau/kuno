<?php
if($argv[1] == 'fxq5Lgd1h9EwKHow12b134PQYu831s5m') {
  require_once '/var/www/src/db/connect.php';
  require_once '/var/www/src/db/fundraisers.php';
  require_once '/var/www/src/db/donations.php';
  require_once '/var/www/src/func/monero_api.php';
  require_once '/var/www/src/func/sanitize.php';
  
  function cron_fetch_donations() {
    $fundraisers = db_select_all_fundraiser_addresses();
    foreach($fundraisers as $fundraiser) {
      sleep(1);
      $transactions = api_get_transactions($fundraiser['address'],$fundraiser['view_key']);
      foreach($transactions as $tx) {
        if(($tx['txid']) && ($tx['amount']) && ($tx['blockheight'])) {
          if((int) db_count_donation_by_txid($tx['txid']) < 1) {
            db_insert_donation(sanitize_alphanumeric($tx['txid']), $fundraiser['address'], (float) $tx['amount'], sanitize_alphanumeric($tx['blockheight']), $tx_key='', $comment='', $email='', time());
          }
        }
      }
      db_update_fundraiser($fundraiser['guid'],'last_synced',time());
    }
  }
  
  cron_fetch_donations();
}
?>