<?php

function api_get_instances() {
  $instances = array(
    'https://xmrchain.net/api',
    'https://xmrblockexplorer.org/api',
    'https://monero.mycryptoapi.com/api',
    'https://explorer.xmr.zelcore.io/api'
  );
  return $instances;
}

function api_count_instances() {
  $instances = api_get_instances();
  return count($instances);
}

function api_validate_address($address,$view_key,$instance=false,$is_retry=false) {
  if($instance !== false) {
    $instance = api_get_instances()[$instance];
  } else {
    $rand = rand(1, (int) api_count_instances()) - 1;
    $instance = api_get_instances()[$rand];
  }
  $url = $instance.'/outputsblocks?address='.$address.'&viewkey='.$view_key.'&limit=5&mempool=0';
  $response = file_get_contents($url);
  $vars = json_decode($response, true);
  if(!$vars) {
    $result = false;
    if(!$is_retry) {
      $rand = min(((int) api_count_instances() - 1), ($rand + 1));
      $retry = api_validate_address($address,$view_key,$instance=$rand,$is_retry=true);
      return $retry;
    } else {
      return $result;
    }
  } else {
    if($vars['status'] == 'error') {
      if(($vars['message']) && (strpos($vars['message'], 'Cant parse monero address') !== false)) {
        $result = 'invalid_address';
      } else if(($vars['message']) && (strpos($vars['message'], 'Cant parse view key') !== false)) {
        $result = 'invalid_view_key';
      } else {
        $result = 'error';
      }
    } else if(($vars['status'] == 'success') && ($vars['data']['view_key'] == $view_key)) {
      $result = 'valid';
    }
  }
  return $result;
}

function api_get_transactions($address,$view_key,$instance=false,$is_retry=false) {
  if($instance !== false) {
    $instance = api_get_instances()[$instance];
  } else {
    $rand = rand(1, (int) api_count_instances()) - 1;
    $instance = api_get_instances()[$rand];
  }
  $url = $instance.'/outputsblocks?address='.$address.'&viewkey='.$view_key.'&limit=5&mempool=0';
  $response = file_get_contents($url);
  $vars = json_decode($response, true);
  if(!$vars) {
    $result = false;
    if(!$is_retry) {
      $rand = min(((int) api_count_instances() - 1), ($rand + 1));
      $retry = api_get_transactions($address,$view_key,$instance=$rand,$is_retry=true);
      return $retry;
    } else {
      return $result;
    }
  } else {
    $result = array();
    if(($vars['status'] == 'success') && ($vars['data']['viewkey'] == $view_key) && ($vars['data']['outputs'])) {
      for($i=0;$i<count($vars['data']['outputs']);$i++) {
        $result[$i]['txid'] = $vars['data']['outputs'][$i]['tx_hash'];
        $result[$i]['amount'] = $vars['data']['outputs'][$i]['amount'] * 0.000000000001;
        $result[$i]['blockheight'] = $vars['data']['outputs'][$i]['block_no'];
      }
    }
  }
  return $result;
}

function api_validate_tx_key($txid,$tx_key,$address,$instance=false,$is_retry=false) {
  if($instance !== false) {
    $instance = api_get_instances()[$instance];
  } else {
    $rand = rand(1, (int) api_count_instances()) - 1;
    $instance = api_get_instances()[$rand];
  }
  $url = $instance.'/outputs?txhash='.$txid.'&address='.$address.'&viewkey='.$tx_key.'&txprove=1';
  $response = file_get_contents($url);
  $vars = json_decode($response, true);
  if(!$vars) {
    $result = false;
    if(!$is_retry) {
      $rand = min(((int) api_count_instances() - 1), ($rand + 1));
      $retry = api_validate_tx_key($txid,$tx_key,$instance=$rand,$is_retry=true);
      return $retry;
    } else {
      return $result;
    }
  } else {
    // $result = 0;
    $result = false;
    if(($vars['status'] == 'success') && ($vars['data']['tx_hash'] == $txid) && ($vars['data']['outputs'])) {
      for($i=0;$i<count($vars['data']['outputs']);$i++) {
        if($vars['data']['outputs'][$i]['match'] == true) {
          // $result = (float) $result + ($vars['data']['outputs'][$i]['amount'] * 0.000000000001);
          $result = true;
        }
      }
    }
  }
  return $result;
}

function regex_validate_address($address) {
  $monero_regex = '/^[4][0-9AB][1-9A-HJ-NP-Za-km-z]{93}$/';
  if(preg_match($monero_regex, $address)) {
    return true;
  } else {
    return false;
  }
}

function regex_validate_view_key($view_key) {
  $monero_regex = '/^[a-f0-9]{64}$/';
  if(preg_match($monero_regex, $view_key)) {
    return true;
  } else {
    return false;
  }
}

?>