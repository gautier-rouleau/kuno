<?php

function encrypt($string,$key) {
  $cipher = "aes-256-gcm";
  $tag_length = 16;
  $key = base64_decode($key);
  $ivlen = openssl_cipher_iv_length($cipher);
  $iv = openssl_random_pseudo_bytes($ivlen,$cstrong);
  $encrypted = openssl_encrypt($string, $cipher, $key, $options=0, $iv, $tag, $aad=null, $tag_length);
  $combo = $iv.$tag.$encrypted;
  $result = base64_encode($combo);
  return $result;
}

function decrypt($string,$key) {
  $cipher = "aes-256-gcm";
  $tag_length = 16;
  $key = base64_decode($key);
  $combo = base64_decode($string);
  $ivlen = openssl_cipher_iv_length($cipher);
  $iv = substr($combo, 0, $ivlen);
  $tag = substr($combo, $ivlen, $tag_length);
  $encrypted = substr($combo, ($ivlen+$tag_length), strlen($combo));
  $result = openssl_decrypt($encrypted, $cipher, $key, $options=0, $iv, $tag);
  return trim($result);
}

function generate_random_key() {
  return base64_encode(openssl_random_pseudo_bytes(32,$cstrong));
}

function generate_key_from_password($password,$salt) {
  $iterations = 2000;
  $chars = 64;
  $pass_key = hash_pbkdf2("sha256", $password, $salt, $iterations, $chars);
  $pass_key = hex2bin($pass_key);
  return base64_encode($pass_key);
}

function encrypt_login($string) {
  $key = 'azVRNVJQZFB6YWdZT2JnRnlIMkptcXNuQlRVU3J1VUk=';
  return encrypt($string, $key);
}

function decrypt_login($string) {
  $key = 'azVRNVJQZFB6YWdZT2JnRnlIMkptcXNuQlRVU3J1VUk=';
  return decrypt($string, $key);
}

function encrypt_string($string) {
  $key = 'cjBkbTdjV1BMRG9Zb1JqemV6eXExcEtEMEpCWXh4Q0E=';
  return encrypt($string, $key);
}

function decrypt_string($string) {
  $key = 'cjBkbTdjV1BMRG9Zb1JqemV6eXExcEtEMEpCWXh4Q0E=';
  return decrypt($string, $key);
}
?>