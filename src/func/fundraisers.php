<?php
require_once '/var/www/src/db/connect.php';
require_once '/var/www/src/db/fundraisers.php';
require_once '/var/www/src/db/donations.php';
require_once '/var/www/src/func/image_upload.php';
require_once '/var/www/src/func/monero_api.php';
require_once '/var/www/src/func/generate_guid.php';
require_once '/var/www/src/func/sanitize.php';
require_once '/var/www/src/func/encrypt.php';

function check_fundraiser_password($guid, $form_password) {
  $guid = sanitize_alphanumeric($guid);
  $fundraiser = db_select_fundraiser_by_guid($guid);
  if(count($fundraiser) < 1) {
    $result = array('status'=>'error','data'=>'Fundraiser not found');
    return $result;
  }
  $db_password = $fundraiser[0]['password'];
  $form_password = sanitize_password($form_password);
  if(!password_verify($form_password, $db_password)) {
    $result = array('status'=>'error','data'=>'Incorrect password');
    return $result;
  } else {
    unset($_SESSION['fundraiser_login']);
    $hash = generate_hash();
    $_SESSION['fundraiser_login'] = encrypt_login($hash.'|'.$guid);
    $redirect = '<meta http-equiv="refresh" content="0; url=https://kuno.bitejo.com/edit-fundraiser/'.$guid.'/">';
    $result = array('status'=>'success','data'=>'<a href="https://kuno.bitejo.com/edit-fundraiser/'.$guid.'/">Click here to edit your fundraiser</a>'.$redirect);
    return $result;
  }
}

function check_fundraiser_session($guid) {
  if(!$_SESSION['fundraiser_login']) {
    return false;
  }
  $decrypt_login = decrypt_login($_SESSION['fundraiser_login']);
  if(!$decrypt_login) {
    unset($_SESSION['fundraiser_login']);
    return false;
  }
  $login_guid = explode('|',trim($decrypt_login))[1];
  $fundraiser = db_select_fundraiser_by_guid($login_guid);
  if(count($fundraiser) < 1) {
    unset($_SESSION['login']);
    return false;
  }
  if($login_guid != $guid) {
    unset($_SESSION['login']);
    return false;
  } else {
    return true;
  }
}

function comment_on_fundraiser($guid, $data) {
  $guid = sanitize_alphanumeric($guid);
  $fundraiser = db_select_fundraiser_by_guid($guid);
  if(count($fundraiser) < 1) {
    $result = array('status'=>'error','data'=>'Fundraiser not found');
    return $result;
  }
  $txid = sanitize_alphanumeric($data['txid']);
  $tx_key = sanitize_alphanumeric($data['tx_key']);
  $email = sanitize_utf8($data['email']);
  $comment = sanitize_utf8($data['comment'],800);
  if((!$txid) || (!$tx_key)) {
    $result = array('status'=>'error','data'=>'Please enter both the TXID and TX key (also known as TX proof)');
    return $result;
  }
  if(!regex_validate_view_key($txid)) {
    $result = array('status'=>'error','data'=>'Invalid TXID');
    return $result;
  }
  if(!regex_validate_view_key($tx_key)) {
    $result = array('status'=>'error','data'=>'Please enter a valid TX key (also known as TX proof)');
    return $result;
  }
  $donation = db_select_donation_by_txid($txid);
  if((!$donation) || ($donation[0]['recipient'] != $fundraiser[0]['address'])) {
    $result = array('status'=>'error','data'=>'TXID not found');
    return $result;
  }
  if(!api_validate_tx_key($txid,$tx_key,$fundraiser[0]['address'])) {
    $result = array('status'=>'error','data'=>'Please enter a valid TX key (also known as TX proof)');
    return $result;
  }
  db_update_donation($donation[0]['txid'],'tx_key',$tx_key);
  if($email) {
    db_update_donation($donation[0]['txid'],'email',$email);
  }
  if($comment) {
    db_update_donation($donation[0]['txid'],'comment',$comment);
  }
  $result = array('status'=>'success','data'=>'Comment sent!');
  return $result;
}

function update_fundraiser_status($action, $guid) {
  $guid = sanitize_alphanumeric($guid);
  if(!check_fundraiser_session($guid)) {
    $result = array('status'=>'error', 'data'=>'Please enter the fundraiser password');
    return $result;
  }
  $compare_fundraiser = db_select_fundraiser_by_guid($guid);
  if(count($compare_fundraiser) < 1) {
    $result = array('status'=>'error','data'=>'Fundraiser not found');
    return $result;
  }
  if($action == 'deactivate_fundraiser') {
    db_update_fundraiser($compare_fundraiser[0]['guid'],'status','deactivated');
    $result = array('status'=>'success','data'=>'Fundraiser deactivated.');
    return $result;
  } else if($action == 'activate_fundraiser') {
    db_update_fundraiser($compare_fundraiser[0]['guid'],'status','active');
    $result = array('status'=>'success','data'=>'Fundraiser activated.');
    return $result;
  } else if($action == 'delete_fundraiser') {
    db_delete_fundraiser($compare_fundraiser[0]['guid']);
    if(($compare_fundraiser[0]['photo']) && ($compare_fundraiser[0]['photo'] != 'none')) {
      $img_details = pathinfo($compare_fundraiser[0]['photo']);
      if($img_details) {
        $img_filename = $img_details['filename'].'.'.$img_details['extension'];
        $thumb_filename = str_replace('.jpeg','_thumb.jpeg',$img_filename);
        delete_uploaded_image($img_filename,'fundraiser','bi0HMROlLZy52I4IlJYmyi9GEOKh10g2');
        delete_uploaded_image($thumb_filename,'fundraiser','bi0HMROlLZy52I4IlJYmyi9GEOKh10g2');
      }
    }
    if(($compare_fundraiser[0]['secondary_photos']) && ($compare_fundraiser[0]['secondary_photos'] != 'none')) {
      $compare_fundraiser[0]['secondary_photos_array'] = json_decode($compare_fundraiser[0]['secondary_photos'], true);
      foreach($compare_fundraiser[0]['secondary_photos_array'] as $secondary_photo) {
        if(($secondary_photo) && ($secondary_photo != 'none')) {
          $img_details = pathinfo($secondary_photo);
          if($img_details) {
            $img_filename = $img_details['filename'].'.'.$img_details['extension'];
            delete_uploaded_image($img_filename,'fundraiser','bi0HMROlLZy52I4IlJYmyi9GEOKh10g2');
          }
        }
      }
    }
    $result = array('status'=>'success','data'=>'Fundraiser deleted.');
    return $result;
  }
}

function publish_fundraiser($data, $action, $guid=false) {
  $errors = array();
  
  if($action == 'edit_fundraiser') {
    $guid = sanitize_alphanumeric($guid);
    if(!check_fundraiser_session($guid)) {
      $result = array('status'=>'error', 'data'=>'Please enter the fundraiser password');
      return $result;
    }
    $compare_fundraiser = db_select_fundraiser_by_guid($guid);
    if(count($compare_fundraiser) < 1) {
      $result = array('status'=>'error','data'=>'Fundraiser not found');
      return $result;
    }
  }
  
  $title = sanitize_utf8($data['title']);
  if((strlen($title) < 1) || (strlen($title) > 80)) {
    $errors[] = 'Title must be between 1-80 characters';
  }
  
  $description = sanitize_utf8($data['description'],8000);
  if(strlen($description) > 8000) {
    $errors[] = 'Description must be shorter than 8000 characters';
  }
  
  $tags = sanitize_utf8($data['tags']);
  if(strlen($tags) > 250) {
    $errors[] = 'Tags must be shorter than 250 characters';
  }
  
  $is_donation_page = ($data['is_donation_page'] == 'yes') ? true : false;
  $url = ($is_donation_page) ? 'donate' : 'fundraiser';
  
  if($is_donation_page) {
    $goal = 0;
  } else {
    $goal = $data['goal'];
    if((!is_numeric($goal)) || ((float) $goal <= 0)) {
      $errors[] = 'Fundraiser goal must be a positive number';
    }
  }
  
  if((strlen($data['email']) > 0) && ($data['email'] != 'none')) {
    if(!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
      $errors[] = 'Please enter a valid email address';
    } else {
      $email = sanitize_email($data['email']);
    }
  } else {
    $email = 'none';
  }
  
  if(($action == 'new_fundraiser') || (($action == 'edit_fundraiser') && (strlen($data['password']) > 1))) {
    if(strlen($data['password']) < 1) {
      $errors[] = 'Please enter a password';
    } else {
      $password = sanitize_password($data['password']);
      $password = password_hash($password, PASSWORD_DEFAULT);
    }
  }
  
  if($action == 'new_fundraiser') {
    $guid = generate_short_guid();
    if((int) db_count_fundraiser_by_guid($guid) > 0) {
      $guid = generate_short_guid();
    }
  }
  
  if(!$errors) {
    $uploaded_photo = 'none';
    if($_FILES['photo']['name']) {
      $fundraiser_filename = 'fundraiser_'.$guid;
      $try_upload = upload_image($fundraiser_filename, 'photo');
      if($try_upload['error']) {
        $errors[] = $try_upload['error'];
      } else {
        $uploaded_photo = 'https://kuno.bitejo.com/assets/uploads/'.hash('sha256', $fundraiser_filename).'.jpeg';
      }
    }
  }
  
  if(!$errors) {
    $secondary_photos = array('1','2','3');
    $secondary_photos_urls = array();
    foreach($secondary_photos as $secondary_photo_num) {
      if($_FILES['secondary_photo_'.$secondary_photo_num]['name']) {
        $try_upload = upload_image($fundraiser_filename.'_secondary_'.$secondary_photo_num, 'secondary_photo_'.$secondary_photo_num);
        if($try_upload['error']) {
          $errors[] = $try_upload['error'];
        } else {
          $secondary_photo_url = 'https://kuno.bitejo.com/assets/uploads/'.hash('sha256', $fundraiser_filename.'_secondary_'.$secondary_photo_num).'.jpeg';
          $secondary_photos_urls[$secondary_photo_num-1] = $secondary_photo_url;
        }
      }
    }
    if($secondary_photos_urls) {
      $secondary_photos_json = json_encode($secondary_photos_urls);
    } else {
      $secondary_photos_urls = 'none';
       $secondary_photos_json = 'none';
    }
  }
  
  if(!$errors) {
    $address = sanitize_alphanumeric($data['address']);
    $view_key = sanitize_alphanumeric($data['view_key']);
    if(($action == 'new_fundraiser') || (($action == 'edit_fundraiser') && ($compare_fundraiser[0]['address'] != $address))) {
      if((int) db_count_fundraiser_by_address($address) > 0) {
        $errors[] = 'There is already a fundraiser/donation page with this Monero address';
      }
    }
    if(($action == 'new_fundraiser') || (($action == 'edit_fundraiser') && (($compare_fundraiser[0]['address'] != $address) || ($compare_fundraiser[0]['view_key'] != $view_key)))) {
      if(!regex_validate_address($address)) {
        $errors[] = 'Invalid Monero address (Please enter your primary address, which starts with 4)';
      }
      if(!regex_validate_view_key($view_key)) {
        $errors[] = 'Invalid view key (Please enter your "private view key")';
      }
      if(!$errors) {
        $validate_address = api_validate_address($address, $view_key);
        if($validate_address == 'invalid_address') {
          $errors[] = 'Invalid Monero address (Please enter your primary address, which starts with 4)';
        } else if($validate_address == 'invalid_view_key') {
          $errors[] = 'Invalid view key (Please enter your "private view key")';
        } else if($validate_address == 'no_connection') {
          $errors[] = 'Could not validate the address (Monero API is offline)';
        }
      }
    }
  }
  
  if(count($errors) > 0) {
    $result = array('status'=>'error','data'=>implode('<br>',$errors));
    return $result;
  }
  
  if($action == 'new_fundraiser') {
    $status = 'active';
    $date = time();
    $publish_fundraiser = db_insert_fundraiser($guid, $address, $view_key, $goal, $title, $description, $tags, $uploaded_photo, $secondary_photos_json, $email, $password, $status='active', $date=time(), $last_synced=0);
    if($publish_fundraiser['status'] != 'success') {
      $result = array('status'=>'error','data'=>'Please refresh and try again.');
      return $result;
    } else {
      $redirect = '<meta http-equiv="refresh" content="0; url=https://kuno.bitejo.com/'.$url.'/'.$guid.'/">';
      $result = array('status'=>'success','data'=>'<a href="https://kuno.bitejo.com/'.$url.'/'.$guid.'/">Your fundraiser is active!</a>'.$redirect);
      return $result;
    }
  } else if($action == 'edit_fundraiser') {
    $changes = array('address','view_key','goal','title','description','tags','email','password');
    foreach($changes as $change) {
      if($compare_fundraiser[0][$change] != ${$change}) {
        db_update_fundraiser($compare_fundraiser[0]['guid'], $change, ${$change});
      }
    }
    if($uploaded_photo != 'none') {
      db_update_fundraiser($compare_fundraiser[0]['guid'], 'photo', $uploaded_photo);
    }
    if($secondary_photos_urls != 'none') {
      db_update_fundraiser($compare_fundraiser[0]['guid'], 'secondary_photos', $secondary_photos_json);
    }
    $result = array('status'=>'success','data'=>'<a href="https://kuno.bitejo.com/'.$url.'/'.$guid.'/">Your fundraiser has been updated.</a>');
    return $result;
  }
}
?>