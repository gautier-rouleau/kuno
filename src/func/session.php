<?php
  session_start([
    'save_handler' => 'files',
    'use_cookies' => true,
    'use_only_cookies' => true,
    'use_strict_mode' => true,
    'cookie_httponly' => true,
    'cookie_secure' => true,
    'cookie_samesite' => 'Strict'
  ]);
  session_regenerate_id();
?>